#!/bin/bash

# La funcion de este script, es introducir datos de forma de argumentos en un fichero csv

# Este argumento if comprueba si el numero de argumentos es menor que 3, sale un error
if [ $# -lt 3 ]; then
    echo "Uso: $0 <nombre> <edad> <ciudad>"
    exit 1
fi

# Los argumentos y el nombre del fichero se guardan en variables
nombre=$1
edad=$2
ciudad=$3
archivo_csv="datos.csv"

# Este argumento if comprueba si el fichero csv no existe, y si no existe lo crea y añade la linea "Nombre,Edad,Ciudad" como la primera linea
if [ ! -e "$archivo_csv" ]; then
    echo "Nombre,Edad,Ciudad" > "$archivo_csv"
fi

# Se mete la informacion dado por los argumentos al fichero csv
echo "$nombre,$edad,$ciudad" >> "$archivo_csv"

# El csv es util para tratar con informacion de una forma mas sencilla

exit 0
