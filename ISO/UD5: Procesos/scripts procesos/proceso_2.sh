#!/bin/bash

echo " Obtenemos el PID"

PID_AUX=$(ps -e | grep firefox | cut -d "?" -f1 | head -n1)

echo " - PID : $PID_AUX"

pstree $PID_AUX

exit 0
