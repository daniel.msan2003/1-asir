#!/bin/bash

if [ "$EUID" -ne 0 ];
then
   echo "ERROR: POR FAVOR EJECUTAR COMO ROOT!!"
   exit 1
fi

fecha=$(date +%Y%m%d)
segundos=$(systemd-analyze | tr -s " " | cut -d "=" -f2 | head -n 1 | cut -d " " -f2 | sed 's/s//;s/\.[[:space:]]*//')

if [ -d "/tmp/loginicio" ];
then
   echo "Existe el directorio: /tmp/loginicio"
   if [ -f "/tmp/loginicio/inicio_$fecha-$segundos.log" ];
   then
      echo "Existe el fichero"

   else
      touch /tmp/loginicio/inicio_$fecha-$segundos.log
      sleep 1
      echo "Se ha creado el fichero: /tmp/loginicio/inicio_$fecha-$segundos.log"
      systemd-analyze critical-chain >> "/tmp/loginicio/inicio_$fecha-$segundos.log"

   fi
   sleep 60

else
   echo "No existe el directorio: /tmp/loginicio"
   echo "Creando ..."
   sleep 1
   mkdir -p /tmp/loginicio
   echo "Creando el fichero: /tmp/loginicio/inicio_$fecha-$segundos.log ..."
   sleep 1
   touch /tmp/loginicio/inicio_$fecha-$segundos.log
   sleep 1
   echo "Se ha creado el fichero: /tmp/loginicio/inicio_$fecha-$segundos.log"
   systemd-analyze critical-chain >> "/tmp/loginicio/inicio_$fecha-$segundos.log"

fi

systemctl restart htg_servicio.service

exit 0
