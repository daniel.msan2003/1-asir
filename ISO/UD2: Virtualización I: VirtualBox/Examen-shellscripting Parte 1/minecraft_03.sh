#!/bin/bash

dia=$(date "+%d")
mes=$(date "+%m")
anyo=$(date "+%Y")

  if [ "$EUID" -ne 0 ];
  then
     echo "ERROR: POR FAVOR EJECUTAR COMO ROOT!!"
     exit 1

  else
     read -p "Dime el nombre de tu Mundo: " mundo
     if [ -d "/srv/MundosMinecraft/$mundo" ];
     then
        cd /srv/MundosMinecraft
        echo "Se va ha crear una copia de seguridad de tu mundo..."
        sleep 1
        zip -r $mundo-$dia-$mes-$anyo.zip $mundo/
        echo "Se ha creado el fichero zip de tu mundo: $mundo_$dia-$mes-$anyo.zip"

     else
        echo "ERROR: el mundo $mundo no existe!!!"
        exit 1
     fi

  fi

exit 0
