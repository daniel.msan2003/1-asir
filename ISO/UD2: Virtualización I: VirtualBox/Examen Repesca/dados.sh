#!/bin/bash

read -p "Dime el tipo de dado que quieres, no uses la letra solo el numero, (D4, D6, D8, D10, D12, D20): " dado
read -p "Dime la cantidad: " cant
read -p "Dime la dificultad:" difi

  if [ $dado = "4" ];
  then
     echo "Has sacado:"
     for n in $(seq 1 $cant);
     do
      let roll=$(shuf -i 1-$dado -n $cant)
      if [ $roll -ge $difi ];
      then
         let res=res+1
         echo "$roll *"
      else
         echo "$roll"
      fi
     done
     echo "Has superado la dificultada $res veces"

  elif [ $dado = "6" ];
  then
     echo "Has sacado:"
     for n in $(seq 1 $cant);
     do
      let roll=$(shuf -i 1-$dado -n $cant)
      if [ $roll -ge $difi ];
      then
         let res=res+1
         echo "$roll *"
      else
         echo "$roll"
      fi
     done
     echo "Has superado la dificultada $res veces"

  elif [ $dado = "8" ];
  then
     echo "Has sacado:"
     for n in $(seq 1 $cant);
     do
      let roll=$(shuf -i 1-$dado -n $cant)
      if [ $roll -ge $difi ];
      then
         let res=res+1
         echo "$roll *"
      else
         echo "$roll"
      fi
     done
     echo "Has superado la dificultada $res veces"

  elif [ $dado = "10" ];
  then
     echo "Has sacado:"
     for n in $(seq 1 $cant);
     do
      let roll=$(shuf -i 1-$dado -n $cant)
      if [ $roll -ge $difi ];
      then
         let res=res+1
         echo "$roll *"
      else
         echo "$roll"
      fi
     done
     echo "Has superado la dificultada $res veces"

  elif [ $dado = "12" ];
  then
     echo "Has sacado:"
     for n in $(seq 1 $cant);
     do
      let roll=$(shuf -i 1-$dado -n $cant)
      if [ $roll -ge $difi ];
      then
         let res=res+1
         echo "$roll *"
      else
         echo "$roll"
      fi
     done
     echo "Has superado la dificultada $res veces"

  elif [ $dado = "20" ];
  then
     echo "Has sacado:"
     for n in $(seq 1 $cant);
     do
      let roll=$(shuf -i 1-$dado -n $cant)
      if [ $roll -ge $difi ];
      then
         let res=res+1
         echo "$roll *"
      else
         echo "$roll"
      fi
     done
     echo "Has superado la dificultada $res veces"

  else
     echo "ERROR: tienes que elegir uno de estos valores: 4, 6, 8, 10, 12, 20"
  fi

exit 0
