#!/bin/bash

RED='\033[0;31m'
NOCOLOR='\033[0m'

if [ "$EUID" -ne 0 ];
then
  echo -e ""${RED}"[ERROR]: "${NOCOLOR}"TIENES QUE EJECUTAR COMO ROOT!!!"
  exit 1

fi

atriles='/srv/orquesta'

if [ -d $atriles ];
then
  echo "El directorio $atriles si existe"

else
  echo "El directorio $atriles no existe, creando..."
  mkdir -p $atriles

fi

if [ -d "$atriles/Sinfonietta" ];
then
  echo "El directorio $atriles/Sinfonietta si existe"
  touch $atriles/Sinfonietta/flautin.txt
  touch $atriles/Sinfonietta/clarinete.txt
  touch $atriles/Sinfonietta/corno.txt
  touch $atriles/Sinfonietta/trompa.txt
  touch $atriles/Sinfonietta/violin.txt
  touch $atriles/Sinfonietta/viola.txt
  touch $atriles/Sinfonietta/chelo.txt
  touch $atriles/Sinfonietta/contrabajo.txt
  touch $atriles/Sinfonietta/bateria.txt
  touch $atriles/Sinfonietta/xilofono.txt
  touch $atriles/Sinfonietta/director.txt
  touch $atriles/Sinfonietta/ElSilencio.txt

else
  echo "El directorio $atriles/Sinfonietta si existe"
  mkdir -p $atriles/Sinfonietta
  touch $atriles/Sinfonietta/flautin.txt
  touch $atriles/Sinfonietta/clarinete.txt
  touch $atriles/Sinfonietta/corno.txt
  touch $atriles/Sinfonietta/trompa.txt
  touch $atriles/Sinfonietta/violin.txt
  touch $atriles/Sinfonietta/viola.txt
  touch $atriles/Sinfonietta/chelo.txt
  touch $atriles/Sinfonietta/contrabajo.txt
  touch $atriles/Sinfonietta/bateria.txt
  touch $atriles/Sinfonietta/xilofono.txt
  touch $atriles/Sinfonietta/director.txt


fi

if [ -d "$atriles/Saturno" ];
then
  echo "El directorio $atriles/Saturno si existe"
  touch $atriles/Saturno/flautin.txt
  touch $atriles/Saturno/clarinete.txt
  touch $atriles/Saturno/corno.txt
  touch $atriles/Saturno/trompa.txt
  touch $atriles/Saturno/violin.txt
  touch $atriles/Saturno/viola.txt
  touch $atriles/Saturno/chelo.txt
  touch $atriles/Saturno/contrabajo.txt
  touch $atriles/Saturno/bateria.txt
  touch $atriles/Saturno/xilofono.txt
  touch $atriles/Saturno/director.txt


else
  echo "El directorio $atriles/Saturno si existe"
  mkdir -p $atriles/Saturno
  touch $atriles/Saturno/flautin.txt
  touch $atriles/Saturno/clarinete.txt
  touch $atriles/Saturno/corno.txt
  touch $atriles/Saturno/trompa.txt
  touch $atriles/Saturno/violin.txt
  touch $atriles/Saturno/viola.txt
  touch $atriles/Saturno/chelo.txt
  touch $atriles/Saturno/contrabajo.txt
  touch $atriles/Saturno/bateria.txt
  touch $atriles/Saturno/xilofono.txt
  touch $atriles/Saturno/director.txt

fi

if [ -d "$atriles/Inacabada" ];
then
  echo "El directorio $atriles/Inacabada si existe"
  touch $atriles/Inacabada/flautin.txt
  touch $atriles/Inacabada/clarinete.txt
  touch $atriles/Inacabada/corno.txt
  touch $atriles/Inacabada/trompa.txt
  touch $atriles/Inacabada/violin.txt
  touch $atriles/Inacabada/viola.txt
  touch $atriles/Inacabada/chelo.txt
  touch $atriles/Inacabada/contrabajo.txt
  touch $atriles/Inacabada/bateria.txt
  touch $atriles/Inacabada/xilofono.txt
  touch $atriles/Inacabada/director.txt


else
  echo "El directorio $atriles/Inacabada si existe"
  mkdir -p $atriles/Inacabada
  touch $atriles/Inacabada/flautin.txt
  touch $atriles/Inacabada/clarinete.txt
  touch $atriles/Inacabada/corno.txt
  touch $atriles/Inacabada/trompa.txt
  touch $atriles/Inacabada/violin.txt
  touch $atriles/Inacabada/viola.txt
  touch $atriles/Inacabada/chelo.txt
  touch $atriles/Inacabada/contrabajo.txt
  touch $atriles/Inacabada/bateria.txt
  touch $atriles/Inacabada/xilofono.txt
  touch $atriles/Inacabada/director.txt

fi

if [ -d "$atriles/Valquirias" ];
then
  echo "El directorio $atriles/Valquirias si existe"
  touch $atriles/Valquirias/flautin.txt
  touch $atriles/Valquirias/clarinete.txt
  touch $atriles/Valquirias/corno.txt
  touch $atriles/Valquirias/trompa.txt
  touch $atriles/Valquirias/violin.txt
  touch $atriles/Valquirias/viola.txt
  touch $atriles/Valquirias/chelo.txt
  touch $atriles/Valquirias/contrabajo.txt
  touch $atriles/Valquirias/bateria.txt
  touch $atriles/Valquirias/xilofono.txt
  touch $atriles/Valquirias/director.txt

else
  echo "El directorio $atriles/Valquirias si existe"
  mkdir -p $atriles/Valquirias
  touch $atriles/Valquirias/flautin.txt
  touch $atriles/Valquirias/clarinete.txt
  touch $atriles/Valquirias/corno.txt
  touch $atriles/Valquirias/trompa.txt
  touch $atriles/Valquirias/violin.txt
  touch $atriles/Valquirias/viola.txt
  touch $atriles/Valquirias/chelo.txt
  touch $atriles/Valquirias/contrabajo.txt
  touch $atriles/Valquirias/bateria.txt
  touch $atriles/Valquirias/xilofono.txt
  touch $atriles/Valquirias/director.txt

fi

if [ -f "users_groups.txt" ];
then
  rm users_groups.txt
  echo "flautin vientomadera,orquesta" >> users_groups.txt
  echo "clarinete vientomadera,orquesta" >> users_groups.txt
  echo "corno vientometal,orquesta" >> users_groups.txt
  echo "trompa vientometal,orquesta" >> users_groups.txt
  echo "violin cuerdas,orquesta" >> users_groups.txt
  echo "viola cuerdas,orquesta" >> users_groups.txt
  echo "chelo cuerdas,orquesta" >> users_groups.txt
  echo "contrabajo cuerdas,orquesta" >> users_groups.txt
  echo "bateria percusion,orquesta" >> users_groups.txt
  echo "xilofono percusion,orquesta" >> users_groups.txt
  echo "director direccion,orquesta" >> users_groups.txt

else
  echo "flautin vientomadera,orquesta" >> users_groups.txt
  echo "clarinete vientomadera,orquesta" >> users_groups.txt
  echo "corno vientometal,orquesta" >> users_groups.txt
  echo "trompa vientometal,orquesta" >> users_groups.txt
  echo "violin cuerdas,orquesta" >> users_groups.txt
  echo "viola cuerdas,orquesta" >> users_groups.txt
  echo "chelo cuerdas,orquesta" >> users_groups.txt
  echo "contrabajo cuerdas,orquesta" >> users_groups.txt
  echo "bateria percusion,orquesta" >> users_groups.txt
  echo "xilofono percusion,orquesta" >> users_groups.txt
  echo "director direccion,orquesta" >> users_groups.txt

fi

while read line;
do
  user=$(echo "$line" | cut -d " " -f1)
  group=$(echo "$line" | cut -d " " -f2 | cut -d "," -f1)
  text="El veloz murciélago hindú comía feliz cardillo y kiwi, mientras la cigüena tocaba el saxofón detrás del palenque de paja....0123456789"
  find $atriles/Sinfonietta $atriles/Saturno $atriles/Inacabada $atriles/Valquirias -type f \( -name "*.txt" \) -exec chmod 640 {} +
  find $atriles/Sinfonietta $atriles/Saturno $atriles/Inacabada $atriles/Valquirias -type f -name "*$user.txt" -exec chown $user:$group {} +
  find $atriles/Saturno -type f \( -name "*violin.txt" -o -name "*viola.txt" -o -name "*chelo.txt" -o -name "*contrabajo.txt" \) -exec chmod 660 {} +
  if [ $user = "director" ];
  then
    find $atriles/Sinfonietta $atriles/Saturno $atriles/Inacabada $atriles/Valquirias -type f \( -name "*.txt" \) -exec setfacl -m g:$group:r {} +
  fi
  find $atriles/Inacabada -type f \( -name "c*" \) -exec chmod o+x {} +
  find $atriles/Valquirias -type f \( -name "c*" \) > Valquirias.txt
  chown nobody:nogroup $atriles/Sinfonietta/ElSilencio.txt
  while read linea;
  do
    echo $text > $linea
  done < Valquirias.txt


done < users_groups.txt

exit 0
