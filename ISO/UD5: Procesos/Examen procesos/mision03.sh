#!/bin/bash

if [ "$EUID" -ne 0 ];
then
   echo "ERROR: POR FAVOR EJECUTAR COMO ROOT!!"
   exit 1
fi

file='procesos.txt'
ps -e | grep "sleep" | tr -s " " | cut -d " " -f2 > $file

while read line;
do
  kill $line
  echo "Se ha matado el proceso sleep con el PID: "$line""

done < $file

exit 0
