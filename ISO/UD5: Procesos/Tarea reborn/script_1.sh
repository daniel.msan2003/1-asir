#!/bin/bash

pid=$(ps -aux --sort=-%cpu,%mem | awk 'NR==2 {print $2}')
cpu=$(ps -p $pid -o %cpu | awk 'NR==2')
ram=$(ps -p $pid -o %mem | awk 'NR==2')
exe=$(readlink /proc/$pid/exe)
dir=$(readlink /proc/$pid/cwd)
com_proc=$(ps -p $pid -o cmd=)

echo "Informe"
echo "_______________________"
echo "PID: $pid"
echo "CPU%: $cpu"
echo "RAM%: $ram"
echo "EXE: $exe"
echo "Commando: $com_proc"
echo "Directorio: $dir"

exit 0
