#!/bin/bash

fecha=$1
hora=$(date +%H)

luna=$(pom $fecha$hora | tr -s " " | cut -d " " -f12 | cut -d "(" -f2 | cut -d "%" -f1)

if [ $# -ne "1" ];
then
   echo "ERROR: tiene que haber solo una argumento!!!"
   echo "El argumento tiene que ser una fecha de nacimiento(la tuya) de formato AAAAMMDD"
   exit 1

else
   if [[ $luna -gt "0" ]] || [[ $luna -lt "19" ]];
   then
      echo "Luna visible: "$luna" %"
      echo "Fase lunar: Luna Nueva"
      echo "Auspicio: Ragabash"

   elif [[ $luna -gt "20" ]] || [[ $luna -lt "39" ]];
   then
      echo "Luna visible: "$luna" %"
      echo "Fase lunar: Luna Creciente"
      echo "Auspicio: Theurge"

   elif [[ $luna -gt "40" ]] || [[ $luna -lt "59" ]];
   then
      echo "Luna visible: "$luna" %"
      echo "Fase lunar: Media Nueva"
      echo "Auspicio: Philodox"

   elif [[ $luna -gt "60" ]] || [[ $luna -lt "84" ]];
   then
      echo "Luna visible: "$luna" %"
      echo "Fase lunar: Luna Gibosa"
      echo "Auspicio: Galliard"

   elif [[ $luna -gt "85" ]] || [[ $luna -lt "100" ]];
   then
      echo "Luna visible: "$luna" %"
      echo "Fase lunar: Luna Nueva"
      echo "Auspicio: Ragabash"

   else
      echo "ERROR"
      exit 1
   fi
fi

exit 0
