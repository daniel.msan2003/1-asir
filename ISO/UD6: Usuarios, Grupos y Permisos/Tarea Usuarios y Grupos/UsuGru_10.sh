#!/bin/bash

file='/etc/group'

while read line;
do
  group=$(echo "$line" | cut -d ":" -f1)
  gid=$(echo "$line" | cut -d ":" -f3)
  if [ "$gid" -gt "1000" ];
  then
     echo "$group tiene un GID de $gid"
  fi

done < "$file"

exit 0
