#!/bin/bash

if [ "$EUID" -ne 0 ];
then
   echo "ERROR: TIENES QUE EJECUTAR COMO ROOT!!"
   exit 1
fi

file1='/etc/passwd'
file2='/etc/shadow'

while read line;
do
  user=$(echo "$line" | cut -d ":" -f1)
  uid=$(echo "$line" | cut -d ":" -f3)
  gid=$(echo "$line" | cut -d ":" -f4)
  group=$(cat /etc/group | cut -d ":" -f3)
  max=$(cat /etc/passwd | cut -d ":" -f3,1 | sort -n | tail -n 1 | awk -F: '{print "Usuario con UID más alto:", $2, "UID:", $1}')
  jedis=$(cat /etc/group | grep jedis | cut -d ":" -f3)
  rebeldes=$(cat /etc/group | grep rebeldes | cut -d ":" -f3)
  imperiales=$(cat /etc/group | grep imperiales | cut -d ":" -f3)
  if [ "$gid" = "$jedis" ];
  then
     echo "$user pertenece a los jedis"

  elif [ "$gid" = "$rebeldes" ];
  then
     echo "$user pertenece a los rebeldes"

  elif [ "$gid" = "$imperiales" ];
  then
     echo "$user pertenece a los imperiales"
  fi

done < "$file1"

echo "$max"
darthvader=$(cat /etc/shadow | grep darthvader | cut -d ":" -f2)
echo "La contrasenya de Darth Vader es $darthvader"

cat /etc/passwd > passwd.txt
echo "Campo 1: nombre del usuario" >> passwd.txt
echo "Campo 2: contrasenya encryptada del usuario" >> passwd.txt
echo "Campo 3: uid del usuario que se asigna a cada usuario por el sistema" >> passwd.txt
echo "Campo 4: gid del grupo, numero del grupo que pertenece cada usuario" >> passwd.txt
echo "Campo 5: informacion adicional del usuario como email o movil" >> passwd.txt
echo "Campo 6: ruta del directorio home del usuario" >> passwd.txt
echo "Campo 7: shell predeterminado del usuario que se inicia cuando el usuario inicia sesión en el sistema" >> passwd.txt

exit 0
