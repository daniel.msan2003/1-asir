#!/bin/bash

ip_client=$1
puerto=$2
user=$3

if [ $# -ne "3" ];
then
    echo "ERROR: tiene que haber tres argumentos!!!"
    echo "El primero con la IP del equipo al que quieres comunicarte"
    echo "La segunda con el numero del puerto"
    echo "El tercero el nombre del usuario"
    exit 1
fi

echo "Bienvenido al chat"
echo "--------------------------------------------------------------------"

nc $1 $2

if [ $? -ne 0 ];
then
    nc -lp $2 | while read line;
    do
      echo "[$user]: $line"
    done
fi

exit 0
