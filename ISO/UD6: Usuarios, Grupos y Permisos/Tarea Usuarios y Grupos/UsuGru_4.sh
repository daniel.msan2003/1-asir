#!/bin/bash

file='/etc/passwd'
arg=$1

while read line;
do
  user=$(echo "$line" | cut -d ":" -f1)
  uid=$(echo "$line" | cut -d ":" -f3)
  if [ "$uid" -lt "$arg" ];
  then
     if [ "$uid" -lt "1000" ];
     then
        echo "$user es un usuario del sistema"

     else
        echo "$user tiene un uid menor que $arg"
     fi
  fi

done < "$file"

exit 0
