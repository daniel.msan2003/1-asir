#!/bin/bash

perm=$1
file=$2

if [ "$EUID" -ne "0" ];
then
   echo "ERROR: tienes que ejecutar como root!!!"
   exit 1
fi

if [ "$#" -ne "2" ];
then
   echo "ERROR: tiene que haber dos argumentos!!!"
   echo "El primer argumento tiene que ser los permisos en formato octal, ejemplo: 712"
   echo "El segundo argumento tiene que ser el nombre del fichero"
   exit 1

else
   chmod $1 $2
fi

exit 0
