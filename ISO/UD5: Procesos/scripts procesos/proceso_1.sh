#!/bin/bash

echo " Obtenemos el PID"

PID_AUX=$(ps -e | grep firefox | cut -d "?" -f1)

echo " - PID : $PID_AUX"

kill -9 $PID_AUX

exit 0
