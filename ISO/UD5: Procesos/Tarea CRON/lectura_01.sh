#!/bin/bash

# Este script muestra cuanto espacio ocupa los usuarios del sistema

# La variable "lista" guarda un comando que muestra solo el nombre de los usuarios del fichero /etc/passwd
lista=$(cut -d: -f1 /etc/passwd)

# El "for" lee el nombre de los usuarios del comando "lista"
for item in $lista; do
    # La variable "usize" guarda un coamndo que muestra el tamaño total del directorio de los usuarios en formato que se puede entender facil sin errores
    usize=$(du -sh /home/$item 2>/dev/null | cut -f1)
    # El "if -n" comprueba si la variable esta definida y no esta vacida
    if [ -n "$usize" ]; then
        # La variable "txt" tiene texto guardado
        txt="El usuario $item, ocupa $usize en su directorio personal (/home/$item)."
        # Este comando va a registrar un mensaje en el sistema con la etiqueta "EspacioUsuarios" con un nivel de gravedad de informacion, y el mensaje va a tener el de la variable "txt"
        logger -t EspacioUsuarios -p local0.info "$txt"
    fi
done
