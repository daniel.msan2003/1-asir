#!/bin/bash

host=$(hostnamectl | cut -d ":" -f2 | head -1)
ip=$(ip a | grep inet | head -n 3 | tail -n 1 | tr -s " " | cut -d " " -f3)
gateway=$(ip r | grep default | cut -d " " -f3)
mac=$(ip a | grep "link/ether" | head -1 | tr -s " " | cut -d " " -f3)
net=$(cat /etc/netplan/*.yaml | grep dhcp4 | cut -d ":" -f2)

echo "Nombre del host: $host"
echo "Direccion IP y mascara: $ip"
echo "Gateway: $gateway"
echo "Direccion MAC: $mac"

if [ $net = "true" ] || [ $net = "yes" ];
then
   echo "El equipo tiene configuracion IP dinamica"

elif [ $net = "false" ] || $net = "no" ];
then
   echo "El equipo tiene configuracion IP estatica"

else
   echo "ERROR: no esta bien configurado el netplan"

fi


exit 0
