#!/bin/bash

RED='\033[0;31m'
NOCOLOR='\033[0m'

if [ "$EUID" -ne 0 ];
then
   echo -e ""${RED}"[ERROR]: "${NOCOLOR}"TIENES QUE EJECUTAR COMO ROOT!!!"
   exit 1

fi

groupadd cuerdas
groupadd vientomadera
groupadd vientometal
groupadd percusion
groupadd direccion
groupadd orquesta

if [ -f "users_groups.txt" ];
then
  rm users_groups.txt
  echo "flautin vientomadera,orquesta" >> users_groups.txt
  echo "clarinete vientomadera,orquesta" >> users_groups.txt
  echo "corno vientometal,orquesta" >> users_groups.txt
  echo "trompa vientometal,orquesta" >> users_groups.txt
  echo "violin cuerdas,orquesta" >> users_groups.txt
  echo "viola cuerdas,orquesta" >> users_groups.txt
  echo "chelo cuerdas,orquesta" >> users_groups.txt
  echo "contrabajo cuerdas,orquesta" >> users_groups.txt
  echo "bateria percusion,orquesta" >> users_groups.txt
  echo "xilofono percusion,orquesta" >> users_groups.txt
  echo "director direccion,orquesta" >> users_groups.txt

else
  echo "flautin vientomadera,orquesta" >> users_groups.txt
  echo "clarinete vientomadera,orquesta" >> users_groups.txt
  echo "corno vientometal,orquesta" >> users_groups.txt
  echo "trompa vientometal,orquesta" >> users_groups.txt
  echo "violin cuerdas,orquesta" >> users_groups.txt
  echo "viola cuerdas,orquesta" >> users_groups.txt
  echo "chelo cuerdas,orquesta" >> users_groups.txt
  echo "contrabajo cuerdas,orquesta" >> users_groups.txt
  echo "bateria percusion,orquesta" >> users_groups.txt
  echo "xilofono percusion,orquesta" >> users_groups.txt
  echo "director direccion,orquesta" >> users_groups.txt

fi

while read line;
do
  user=$(echo "$line" | cut -d " " -f1)
  group=$(echo "$line" | cut -d " " -f2)
  test=$(cat /etc/passwd | cut -d ":" -f1 | grep -w $user | wc -l)
  if [ "$test" = "0" ];
  then
    echo "El usuario $user no existe"
    useradd $user -m -s "/bin/bash" -G $group

  else
    echo "El usuario $user si existe"
    usermod -a -G $group $user

  fi

done < users_groups.txt

exit 0
