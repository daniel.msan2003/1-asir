#!/bin/bash

  if [ "$EUID" -ne 0 ];
  then
     echo "ERROR: POR FAVOR EJECUTAR COMO ROOT!!"
     exit 1

  else
    read -p "Dime el nombre de tu mundo Minecraft que existe: " mundo
    read -p "Dime el nombre del archivo que quieres copiar a tu escritorio: " arch
    read -p "Dime la ruta de tu escritorio: " esc
    if [ -d "/srv/MundosMinecraft/$mundo" ];
    then
      cd /srv/MundosMinecraft/$mundo
      echo "Copiando el archivo $arch a tu escritorio ..."
      sleep 1
      cp $arch $esc

    else
       echo "ERROR: el mundo $mundo no existe"
       exit 1

    fi

  fi

exit 0
