#!/bin/bash

if [ "$EUID" -ne 0 ];
then
   echo "ERROR: TIENES QUE EJECUTAR COMO ROOT!!"
   exit 1
fi

typhoon=$(docker inspect --format '{{.State.Running}}' Typhoon)
delta=$(docker inspect --format '{{.State.Running}}' Delta)
vanguard=$(docker inspect --format '{{.State.Running}}' Vanguard)

if [ $typhoon = "true" ];
then
   echo "En marcha"
   docker start Typhoon
   docker exec Typhoon mv ./puestaEnMarcha/MisilPoseidon01_OFF ./puestaEnMarcha/MisilPoseidon01_ON
   docker exec Typhoon mv ./puestaEnMarcha/MisilPoseidon02_OFF ./puestaEnMarcha/MisilPoseidon02_ON
   docker exec Typhoon mv ./puestaEnMarcha/MisilPoseidon03_OFF ./puestaEnMarcha/MisilPoseidon03_ON

else
   echo "ERROR: el contenedor Typhoon no esta en marcha"
fi

if [ $delta = "true" ];
then
   echo "En marcha"
   docker start Delta
   docker exec Delta mv ./puestaEnMarcha/Lanzatorpedo01_OFF ./puestaEnMarcha/Lanzatorpedo01_ON
   docker exec Delta mv ./puestaEnMarcha/Lanzatorpedo02_OFF ./puestaEnMarcha/Lanzatorpedo02_ON
   docker exec Delta mv ./puestaEnMarcha/Lanzatorpedo03_OFF ./puestaEnMarcha/Lanzatorpedo03_ON
   docker exec Delta mv ./puestaEnMarcha/Lanzatorpedo04_OFF ./puestaEnMarcha/Lanzatorpedo04_ON
   docker exec Delta mv ./puestaEnMarcha/MisileBalisticos01_OFF ./puestaEnMarcha/MisileBalisticos01_ON
   docker exec Delta mv ./puestaEnMarcha/MisileBalisticos02_OFF ./puestaEnMarcha/MisileBalisticos02_ON
   docker exec Delta mv ./puestaEnMarcha/MisileBalisticos03_OFF ./puestaEnMarcha/MisileBalisticos03_ON
   docker exec Delta mv ./puestaEnMarcha/MisileBalisticos04_OFF ./puestaEnMarcha/MisileBalisticos04_ON

else
   echo "ERROR: el contenedor delta no esta en marcha"
fi

if [ $vanguard = "true" ];
then
   echo "En marcha"
   docker start Vanguard
   docker exec Vanguard mv ./puestaEnMarcha/MisilTridentII01_OFF ./puestaEnMarcha/MisilTridentII01_ON
   docker exec Vanguard mv ./puestaEnMarcha/MisilTridentII02_OFF ./puestaEnMarcha/MisilTridentII02_ON
   docker exec Vanguard mv ./puestaEnMarcha/MisilTridentII03_OFF ./puestaEnMarcha/MisilTridentII03_ON
   docker exec Vanguard mv ./puestaEnMarcha/MisilTridentII04_OFF ./puestaEnMarcha/MisilTridentII04_ON
   docker exec Vanguard mv ./puestaEnMarcha/MisilTridentII05_OFF ./puestaEnMarcha/MisilTridentII05_ON
   docker exec Vanguard mv ./puestaEnMarcha/PrototipoSecreto01_OFF ./puestaEnMarcha/PrototipoSecreto01_ON

else
   echo "ERROR: el contenedor Vanguard no esta en marcha"
fi

exit 0
