#!/bin/bash

RED='\033[0;31m'
NOCOLOR='\033[0m'

if [ "$EUID" -ne 0 ];
then
  echo -e ""${RED}"[ERROR]: "${NOCOLOR}"TIENES QUE EJECUTAR COMO ROOT!!!"
  exit 1

fi

file='/srv/seniara/pokeSen.csv'
password='seniara'
num_line=$(cat "$file" | wc -l)
let num_line=num_line-1

cat "$file" | tail -n "$num_line" | while read line;
do
  name=$(echo "$line" | cut -d "," -f2 | cut -d " " -f1 | tr '[:upper:]' '[:lower:]')
  full_name=$(echo "$line" | cut -d "," -f2)
  types=$(echo "$line" | cut -d "," -f3)
  legen=$(echo "$line" | cut -d "," -f13)
  user=$(cat /etc/passwd | cut -d ":" -f1 | grep -w $name | wc -l)
  group=$(cat /etc/group | cut -d ":" -f1 | grep -w $types | wc -l)
  if [ "$group" = 0 ];
  then
    groupadd $types
  fi

  if [ "$legen" = "True" ];
  then
    if [ "$user" = 0 ];
    then
      useradd "$name" -G "$types" -c "$full_name"
      echo -e "$password\n$password" | passwd $name
    fi
  fi

done

cant_sen=$(cat "$file" | cut -d "," -f2 | wc -w)
cant_types=$(cat "$file" | cut -d "," -f3 | sort | uniq | wc -w)
echo "______________________________________________________"
echo "Cantidad de Senomons creados: $cant_sen"
echo "Cantidad de Tipos de senomon creados: $cant_types"

exit 0
