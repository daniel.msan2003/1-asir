#!/bin/bash

file='/etc/group'
arg=$1

while read line;
do
  group=$(echo "$line" | cut -d ":" -f1)
  users=$(echo "$line" | cut -d ":" -f4)
  if [[ "$group" = "$arg" ]] && [[ -z "$users" ]];
  then
     echo "El grupo $group no tiene usuarios"

  elif [ "$group" = "$arg" ];
  then
     echo "El grupo $group tiene los usuarios: $users"

  fi

done < "$file"

exit 0
