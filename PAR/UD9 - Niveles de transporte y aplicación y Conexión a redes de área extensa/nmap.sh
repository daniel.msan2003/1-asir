#!/bin/bash

if [ "$EUID" -ne 0 ];
then
    echo "ERROR: TIENES QUE EJECUTAR COMO ROOT!!"
    exit 1
fi

patron="^([0-9]{1,3}\.){3}[0-9]{1,3}-[0-9]{1,3}$"
rango=$1

if [ $# -eq "1" ];
then
    if [[ $rango =~ $patron ]];
    then
        nmap -sP $rango > IPs.txt
        echo "Direcciones IPS en marcha:"
        echo ""
        cat IPs.txt | grep -oE '\b([0-9]{1,3}\.){3}[0-9]{1,3}\b' | tr "\n" " "
    else
        echo "ERROR!!!"
        echo "El rango tiene que tener esta estructura: 192.168.4.1-200"
        exit 1
    fi

else
    echo "ERROR: tiene que haber solo un argumento"
    echo "El rango tiene que tener esta estructura: 192.168.4.1-200"
    exit 1
fi

exit 0
