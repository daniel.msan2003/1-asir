#!/bin/bash

  if [ -e "natural_N.txt" ];
  then
     rm natural_N.txt
  fi

  read -p "Dame un numero: " num

  for num2 in $(seq 1 $num);
  do
    let res=num+num2
    echo "$num + $num2 = $res" > natural_N.txt
  done

exit 0
