#!/bin/bash

if [ "$EUID" -ne 0 ];
then
    echo "ERROR: TIENES QUE EJECUTAR COMO ROOT!!"
    exit 1
fi

whereami=pwd

if [ -f "casas.ldif" ];
then
    echo "Existe el fichero casas.ldif"
    if [ -r "$whereami/casas.ldif" ];
    then
        echo "El fichero casas.ldif se puede leer en el directorio actual"
        ldapadd -x -H ldap://192.168.10.10 -D "cn=admin,dc=valencia,dc=iso,dc=com" -W -f casas.ldif

    else
        echo "El fichero casas.ldi no se puede leer en el directorio actual"
        chmod 777 casas.ldif
        ldapadd -x -H ldap://192.168.10.10 -D "cn=admin,dc=valencia,dc=iso,dc=com" -W -f casas.ldif
    fi

else
    echo "No existe el fichero casas.ldif"
fi

if [ -f "miembros.ldif" ];
then
    echo "Existe el fichero miembros.ldif"
    if [ -r "$whereami/miembros.ldif" ];
    then
        echo "El fichero miembros.ldif se puede leer en el directorio actual"
        ldapadd -x -H ldap://192.168.10.10 -D "cn=admin,dc=valencia,dc=iso,dc=com" -W -f miembros.ldif

    else
        echo "El fichero miembros.ldif no se puede leer en el directorio actual"
        chmod 777 miembros.ldif
        ldapadd -x -H ldap://192.168.10.10 -D "cn=admin,dc=valencia,dc=iso,dc=com" -W -f miembros.ldif
    fi

else
    echo "No existe el fichero miembros.ldif"
fi

exit 0
