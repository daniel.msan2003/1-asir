#!/bin/bash

if [ "$EUID" -ne 0 ];
then
   echo "ERROR: POR FAVOR EJECUTAR COMO ROOT!!"
   exit 1
fi

line=$(cat tiempos.log | cut -d "," -f2 | tail -n15)
media=$(cat tiempos.log | cut -d "," -f2 | tail -n15 | wc -l)

if [ -d "/var/examenISO" ];
then
   echo "El directorio /var/examenISO ya existe"
   if [ -f "/var/examenISO/tiempos.log" ];
   then
      echo "El fichero tiempos.log ya existe"
      rm /var/examenISO/tiempos.log
      sleep 1
      for n in $line;
      do
        num=$((num+n))
        res=$((num/media))
      done
      echo "La media de tiempo de arranque del sistema ha sido $res segundos." > /var/examenISO/tiempos.log

   else
      echo "El fichero tiempos.log no existe"
      echo "Creando..."
      for n in $line;
      do
        num=$((num+n))
        res=$((num/media))
      done
      echo "La media de tiempo de arranque del sistema ha sido $res segundos." > /var/examenISO/tiempos.log
   fi

else
   echo "El directorio /var/examenISO no existe"
   sleep 1
   echo "Creando..."
   sleep 1
   mkdir -p /var/examenISO
   for n in $line;
   do
     num=$((num+n))
     res=$((num/media))
   done
   echo "La media de tiempo de arranque del sistema ha sido $res segundos." /var/examenISO/tiempos.log
fi

exit 0
