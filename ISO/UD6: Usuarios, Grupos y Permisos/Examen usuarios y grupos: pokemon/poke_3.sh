#!/bin/bash

RED='\033[0;31m'
NOCOLOR='\033[0m'

if [ "$EUID" -ne 0 ];
then
  echo -e ""${RED}"[ERROR]: "${NOCOLOR}"TIENES QUE EJECUTAR COMO ROOT!!!"
  exit 1

fi

file='/srv/seniara/pokeSen.csv'

while read line;
do
  user=$(whoami)
  usu=$(echo "$line" | grep -w "$user" | cut -d "," -f2)
  type1=$(echo "$line" | cut -d "," -f3)
  type2=$(echo "$line" | cut -d "," -f4)
  if [ "$usu" = "$user" ];
  then
    echo "Eres de tipo $type1 y $type2"
  fi

done < $file

exit 0
