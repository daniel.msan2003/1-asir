#!/bin/bash

adivinar=$((RANDOM % 100 + 1))

intentos=0

echo "Intenta adivinar el numero secreto entre 1 y 100."

while [ "$respuesta" != "$adivinar" ];
do
    read -p "Ingresa un numero: " respuesta


    if (( respuesta < 1 || respuesta > 100 ));
    then
        echo "ERROR: $respuesta no esta en el rango de 1 a 100"
        continue
    fi

    ((intentos++))

    if (( respuesta == adivinar ));
    then
        echo "Felicidades, has adivinado el numero secreto $adivinar en $intentos intentos!"
        break

    elif (( respuesta > adivinar ));
    then
        echo "El numero ingresado es demasiado alto. Intenta de nuevo."

    elif (( respuesta < adivinar ));
    then
        echo "El numero ingresado es demasiado bajo. Intenta de nuevo."

    else
       echo "ERROR: $respuesta no esta en el rango"

    fi

done

exit 0
