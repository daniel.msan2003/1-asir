#!/bin/bash

pasta=0

if [ "$EUID" -ne 0 ];
then
   echo "ERROR: POR FAVOR EJECUTAR COMO ROOT!!"
   exit 1
fi

fecha=$(date +%Y%m%d)

if [ -d "/var/examenISO" ];
then
   echo "El directorio /var/examenISO ya existe"

else
   echo "El directorio /var/examenISO no existe, ya se ha creado"
   mkdir -p /var/examenISO/

fi

while [ "$pasta" = 0 ];
do
  dmesg | grep -i "Manufacturer" | grep -i "usb" | cut -d ":" -f3 | tr -d " " > /var/examenISO/volcadoUSB_$fecha.log
  sleep 300

done

exit 0
