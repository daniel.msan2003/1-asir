#!/bin/bash

if [ "$EUID" -ne 0 ];
then
   echo "ERROR: TIENES QUE EJECUTAR COMO ROOT!!"
   exit 1
fi

prisioneros=$(find Estrella_de_la_Muerte/prision/ -type f \( -name "*prisionero*" -o -name "*rebelde*" \))
logs=$(cat Estrella_de_la_Muerte/logs/logs_sistema.txt | cut -d "," -f5)
user=$(cat Estrella_de_la_Muerte/prision/registro_visitas.txt | tail -n 1 | cut -d "|" -f3)

echo "Ficheros con la palabra “prisionero” o “rebelde” en su nombre:"
echo "$prisioneros"
echo "_______________________________________________________________________________"
echo "Entradas relacionadas con los prisioneros:"
echo "$logs"
echo "_______________________________________________________________________________"
echo "Usuario que ha accedido a la zona de la prision een la ultima hora:"
echo "$user"

echo "Comprimiendo los directorios hackeados..."
sleep 1
zip -r hackeado.zip Estrella_de_la_Muerte/* Rebeldes/*
echo "Asignando al grupo rebeldes"
sleep 1
chgrp rebeldes hackeado.zip
echo "Borrando los directorios Estrella de la Muerte y Rebeldes..."
sleep 1
rm -r Estrella_de_la_Muerte/
rm -r Rebeldes/

exit 0
