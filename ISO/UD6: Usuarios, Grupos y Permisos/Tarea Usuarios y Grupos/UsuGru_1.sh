#!/bin/bash

file='/etc/passwd'

while read line;
do
  shell=$(echo "$line" | cut -d ":" -f7)
  if [[ ! -z $shell ]];
  then
     echo "$line"
  fi

done < "$file"

exit 0
