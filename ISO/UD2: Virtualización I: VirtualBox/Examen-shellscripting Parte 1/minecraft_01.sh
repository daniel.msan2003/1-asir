#!/bin/bash

  if [ "$EUID" -ne 0 ];
  then
     echo "ERROR: POR FAVOR EJECUTAR COMO ROOT!!"
     exit 1

  else
    read -p "Dime el nombre de tu mundo de Minecraft: " nom
    if [ -d "/srv/MundosMinecraft/$nom" ];
    then
       echo "ERROR: ESTE MUNDO YA EXISTE!!!"

    else
      echo "Creando el Mundo: $nom ..."
      sleep 1
      mkdir -p /srv/MundosMinecraft/$nom
      cd /srv/MundosMinecraft/$nom
      sleep 1
      mkdir region data playerdata saves
      touch level.dat stats.dat
      sleep 1
      echo "Ya se ha creado tu mundo"

    fi

  fi


exit 0
