#!/bin/bash

file='/etc/group'
count=0

while read line;
do
  group=$(echo "$line" | cut -d ":" -f1)
  if [[ ! -z "$group" ]];
  then
     let count=count+1

  fi

done < "$file"

echo "Hay $count grupos en el sistema"

exit 0
