#!/bin/bash

if [ "$EUID" -ne 0 ];
then
    echo "ERROR: TIENES QUE EJECUTAR COMO ROOT!!"
    exit 1
fi

if [ -d "/maquina" ];
then
    echo "El directorio /maquina ya existe"

else
    mkdir /maquina
fi

if [[ -d "/maquina/astronomia" ]] || [[ ! -d "/maquina/astronomia" ]];
then
    rm -r /maquina/astronomia
    mkdir /maquina/astronomia
    dd if=/dev/urandom of=/maquina/astronomia/fichero_kirch.txt bs=4096 count=10000
    dd if=/dev/urandom of=/maquina/astronomia/fichero_minaflemming.txt bs=4096 count=10000
fi

if [[ -d "/maquina/biologia" ]] || [[ ! -d "/maquina/biologia" ]];
then
    rm -r /maquina/biologia
    mkdir /maquina/biologia
    dd if=/dev/urandom of=/maquina/biologia/fichero_maryanning.txt bs=4096 count=20000
fi

if [[ -d "/maquina/matematica" ]] || [[ ! -d "/maquina/matematica" ]];
then
    rm -r /maquina/matematica
    mkdir /maquina/matematica
    dd if=/dev/urandom of=/maquina/matematica/fichero_ada.txt bs=4096 count=40000
    dd if=/dev/urandom of=/maquina/matematica/fichero_emmynoether_fisica.txt bs=4096 count=40000

fi

if [[ -d "/maquina/fisica" ]] || [[ ! -d "/maquina/fisica" ]];
then
    rm -r /maquina/fisica
    mkdir /maquina/fisica
    ln -s /maquina/matematica/fichero_emmynoether_fisica.txt /maquina/fisica/enlace_emmynoether.link
    dd if=/dev/urandom of=/maquina/fisica/fichero_emmynoether.datos bs=4096 count=30000
    dd if=/dev/urandom of=/maquina/fisica/fichero_emmynoether.secreto bs=4096 count=30000
    dd if=/dev/urandom of=/maquina/fisica/fichero_emmynoether.txt bs=4096 count=30000
    dd if=/dev/urandom of=/maquina/fisica/fichero_lisameither.txt bs=4096 count=30000
fi

if [[ -d "/maquina/quimica" ]] || [[ ! -d "/maquina/quimica" ]];
then
    rm -r /maquina/quimica
    mkdir /maquina/quimica
    dd if=/dev/urandom of=/maquina/quimica/fichero_mariecurie.txt bs=4096 count=50000
fi

exit 0


