#!/bin/bash

file='/etc/passwd'
count=0

while read line;
do
  uid=$(echo "$line" | cut -d ":" -f3)
  gid=$(echo "$line" | cut -d ":" -f4)
  if [ "$uid" != "$gid" ];
  then
     let count=count+1

  fi

done < "$file"

echo "Hay $count usuarios que tienen un grupo diferente al suyo"

exit 0

