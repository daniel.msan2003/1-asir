#!/bin/bash

  while [ "$ans" != "0" ];
  do
    echo "+---------------------------------------------+"
    echo "        MONTE        DEL        DESTINO"
    echo "+---------------------------------------------+"
    echo "      1. Tres anillos para los Elfos."
    echo "      2. Siete anillos para los Enanos."
    echo "      3. Nueve anillos para los Humanos."
    echo ""
    echo "      0. Apagar "
    echo "+---------------------------------------------+"
    read -p "Que tesoro desea forjar? (1-3): " ans
    if [ "$ans" = "1" ];
    then
       if [ -d "./forja/elfos" ];
       then
         echo "El directorio ./forja/elfos ya existe, borrando..."
         sleep 1
         rm -r ./forja/elfos
         echo "Ahora creando el directorio"
         sleep 1
         mkdir -p ./forja/elfos
         for f in $(seq 1 3);
         do
          touch ./forja/elfos/anilloElfos$f.txt
         done

       else
          echo "Creando el directorio ./forja/elfos..."
          sleep 1
          mkdir -p ./forja/elfos
         for e in $(seq 1 3);
         do
          touch ./forja/elfos/anilloElfos$f.txt
         done

       fi


    elif [ "$ans" = "2" ];
    then
       if [ -d "./forja/enanos" ];
       then
         echo "El directorio ./forja/enanos ya existe, borrando..."
         sleep 1
         rm -r ./forja/enanos
         echo "Ahora creando el directorio"
         sleep 1
         mkdir -p ./forja/enanos
         for e in $(seq 1 7);
         do
          touch ./forja/enanos/anilloEnano$e.txt
         done

       else
          echo "Creando el directorio ./forja/enanos..."
          sleep 1
          mkdir -p ./forja/enanos
         for e in $(seq 1 7);
         do
          touch ./forja/enanos/anilloEnano$e.txt
         done

       fi

    elif [ "$ans" = "3" ];
    then
     if [ -d "./forja/humanos" ];
     then
       echo "El directorio ./forja/humanos ya existe, borrando..."
       sleep 1
       rm -r ./forja/humanos
       echo "Ahora creando el directorio"
       sleep 1
       mkdir -p ./forja/humanos
       sleep 1
       for h in $(seq 1 9);
       do
        touch ./forja/humanos/anilloHumano$h.txt
       done

     else
       echo "Creando el directorio ./forja/humanos..."
       sleep 1
       mkdir -p ./forja/humanos
       for h in $(seq 1 9);
       do
        touch ./forja/humanos/anilloHumano$h.txt
       done

     fi
    fi

    if [ -d "./forja/elfos" ] || [ -d "./forja/enanos" ] || [ -d "./forja/humanos" ]
    then
       echo "+---------------------------------------------+"
       echo "        MONTE        DEL        DESTINO"
       echo "+---------------------------------------------+"
       echo "      1. Tres anillos para los Elfos."
       echo "      2. Siete anillos para los Enanos."
       echo "      3. Nueve anillos para los Humanos."
       echo ""
       echo "      9. Anillo Unico"
       echo ""
       echo "      0. Apagar "
       echo "+---------------------------------------------+"
       read -p "Deseas forjar el Anillo Unico para unirlos en las tinieblas? (S/s): " ani
       if [ "$ani" = "s" ] || [ "$ani" = "S" ];
       then
         touch ./forja/anilloUnico.txt
         exit
       fi
    fi
  done

exit 0
