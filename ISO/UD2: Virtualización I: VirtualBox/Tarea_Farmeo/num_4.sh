#!/bin/bash

intentos=0

  while [ "$opcion" != "n" ];
  do
    read -p "Indique la unidad de media (C/F): " uni
    read -p "Indique la cantidad: " can

    if [ "$uni" = "c" ] || [ "$uni" = "C" ];
    then
       far=$(( (can * 9) / 5 + 32))
       echo "$can Celsius --> $far Farenheit"
       read -p "Desea otra opcion? (s/n): " opcion
       if [ "$opcion" = "s" ] || [ "$opcion" = "S" ];
       then
          intentos=$((intentos+1))

       elif [ "$opcion" = "n" ] || [ "$opcion" = "N" ];
       then
          intentos=$((intentos+1))
          echo "Se han realizado $intentos conversiones." > conversion.log

       else
          echo "ERROR: esa no es una opcion"
	  exit 1

       fi

    elif [ "$uni" = "f" ] || [ "$uni" = "F" ];
    then
       cel=$(( (can - 32) * 5 / 9 ))
       echo "$can Farenheit --> $cel Celsius"
       read -p "Desea otra opcion? (s/n): " opcion
       if [ "$opcion" = "s" ] || [ "$opcion" = "S" ];
       then
          intentos=$((intentos+1))

       elif [ "$opcion" = "n" ] || [ "$opcion" = "N" ];
       then
          intentos=$((intentos+1))
          echo "Se han realizado $intentos conversiones." > conversion.log

       else
          echo "ERROR: esa no es una opcion"
          exit 1

       fi

    else
       echo "ERROR: no es una opcion!!!"
       exit 1

    fi

  done

exit 0
