#!/bin/bash

# La variable "fich" contiene el fichero "contaDirs.txt"
fich="contaDirs.txt"

# La variable "dirRaiz" contiene la ruta del home del usuario actual con el directorio "Contabilidad"
dirRaiz="$HOME/Contabilidad"

# El while lee las linea del fichero 1 por 1
while read -r linea; do
# Crea el directorio con la variable "dirRaiz", el directorio empezara con el HOME del usuario actual, seguido con el subdirectorio "Contabilidad", y de ultimo tambien crea 7 subdirectorios con la variable "$linea"
mkdir -p "$dirRaiz/$linea"
# La variable "codError" guarda el resultado de cada eejecucion
codError=$?
# El done cierra el while y lo redirige a la variable que tiene el fichero "contaDirs.txt"
done < "$fich"

# Se sale con el codigo error que sera un numero
exit $codError
