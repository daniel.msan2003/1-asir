#!/bin/bash

if [ "$EUID" -ne 0 ];
then
    echo "ERROR: TIENES QUE EJECUTAR COMO ROOT!!"
    exit 1
fi

dir_origen=$1
dir_destino=$2

if [ $# = "2" ];
then
    ln $dir_origen $dir_destino
else
    echo "ERROR: tiene que haber dos argumentos y tienen que ser una ruta absoluta!!!"
    echo "El primer argumento tiene que ser el directorio de origen"
    echo "Y el segundo argumento tiene que ser el directorio de destino"
    exit 1
fi

exit 0
