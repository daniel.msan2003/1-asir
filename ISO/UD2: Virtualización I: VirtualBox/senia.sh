#!/bin/bash

if [ -d "LaSenia" ];
then
   rm -r LaSenia
fi

mkdir LaSenia

cd LaSenia

mkdir dptoInf dptoMat dptoBio

if [ -d "dptoInf" ];
then
   echo "El directorio de informatica si existe"

fi

mkdir -p dptoInf/AulaInf01 dptoInf/AulaInf02 dptoInf/Aula23

if [ -d "dptoInf/AulaInf01" ];
then
   echo "El directorio de informatica si tiene AulaInf01"

fi

if [ -d "dptoInf/AulaInf02" ];
then
   echo "El directorio de informatica si tiene AulaInf02"

fi


if [ -d "dptoInf/Aula23" ];
then
   echo "El directorio de informatica si tiene la Aula23"

fi

touch dptoInf/AulaInf01/alex.txt dptoInf/AulaInf02/jesus.txt dptoInf/Aula23/jose.txt

if [ -f "dptoInf/AulaInf01/alex.txt" ];
then
   echo "El fichero alex.txt si existe dentro de AulaInf01"

fi

if [ -f "dptoInf/AulaInf02/jesus.txt" ];
then
   echo "El fichero jesus.txt si existe dentro de AulaInf02"

fi

if [ -f "dptoInf/Aula23/jose.txt" ];
then
   echo "El fichero jose.txt si existe dentro de Aula23"

fi

mkdir -p dptoMat/Aula23 dptoMat/Aula21

if [ -d "dptoMat" ];
then
   echo "El directorio de matematicas si existe"

fi

if [ -d "dptoMat/Aula23" ];
then
   echo "El directorio de matematicas si tiene la Aula23"

fi

if [ -d "dptoMat/Aula21" ];
then
   echo "El directorio de matematicas si tiene la Aula21"

fi


touch dptoMat/Aula21/javi.txt

if [ -f "dptoMat/Aula21/javi.txt" ];
then
   echo "El fichero javi.txt si existe dentro de Aula21"

fi

mkdir -p dptoBio/Aula22 dptoBio/Aula21 dptoBio/Aula23

if [ -d "dptoBio" ];
then
   echo "El directorio de biologia si existe"

fi

if [ -d "dptoBio/Aula22" ];
then
   echo "El directorio de biologia si tiene la Aula22"

fi

if [ -d "dptoBio/Aula21" ];
then
   echo "El directorio de biologia si tiene la Aula21"

fi

if [ -d "dptoBio/Aula23" ];
then
   echo "El directorio de biologia si tiene la Aula23"

fi

touch dptoBio/Aula22/domingo.txt

if [ -f "dptoBio/Aula22/domingo.txt" ];
then
   echo "El fichero domingo.txt si existe dentro de Aula22"

fi
