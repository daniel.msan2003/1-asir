#!/bin/bash

# El script verifica los permisos de escritura y ejecucion para otros usuarios en archivos y directorios dentro de un directorio dado, emitiendo advertencias en caso de que esten presentes

# Asigna el primer argumento a la variable "directorio"
directorio="$1"

# Comprueba si el directorio existe, y si no existe muestra un mensaje de error
if [ ! -d "$directorio" ]; then
  echo "El directorio '$directorio' no existe."
  exit 1
fi

# Itera sobre todos los archivos y directorios encontrados en el directorio dado
for archivo in $(find "$directorio" -type f -o -type d); do

  # Obtiene los permisos del archivo o directorio actual
  permisos=$(stat -c "%a" "$archivo")

  # Extrae el tercer dígito de los permisos, que corresponde a los otros usuarios
  otros=$(echo $permisos | cut -c 3)

  # Comprueba si los permisos de los otros usuarios incluyen escritura (2, 3, 6, 7) y muestra una mensaje si es asi
  if [ $otros -eq 2 ] || [ $otros -eq 3 ] || [ $otros -eq 6 ] || [ $otros -eq 7 ]; then
    echo "Advertencia! Permisos de escritura para 'otros' en $archivo"
  fi

  # Comprueba si los permisos de los otros usuarios incluyen ejecucion (1, 3, 5, 7) y muestra un mensaje si es asi
  if [ $otros -eq 1 ] || [ $otros -eq 3 ] || [ $otros -eq 5 ] || [ $otros -eq 7 ]; then
    echo " Advertencia! Permisos de ejecucion para 'otros' en $archivo"
  fi

done
