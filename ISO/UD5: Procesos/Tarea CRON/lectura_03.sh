#!/bin/bash

# Este script hace una copia de seguridad del home del usuario actual

# La variable "orig" tiene la ruta del home del usuario actual
orig="$HOME"

# La variable "dest" tiene la ruta donde se hace los backups
dest="/srv/backup"

# Comprueba si el directorio home existe
if [ ! -d "$orig" ]; then
    echo "Error: El directorio de origen no existe."
    exit 1
fi

# Comprueba que si el diretorio no existe, se crea
if [ ! -d "$dest" ]; then
    mkdir -p "$dest"
fi

# Variable con la fecha
tmsp=$(date +"%Y%m%d_%H%M%S")

# Hace una copia de seguridad del home del usuario actual con la fecha y hora a la destinacion de la variable "dest"
cp -r "$orig" "$dest/directorio_respaldado_$tmsp"

# Comprueba si la copia de seguridad ha sido un exito o un fallo, y manda un mensaje al log del sistema
if [ $? -eq 0 ]; then
    echo "Error al realizar la copia de respaldo." | logger
else
    echo "Copia de respaldo exitosa." | logger
fi
