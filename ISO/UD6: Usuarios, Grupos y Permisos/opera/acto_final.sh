#!/bin/bash

RED='\033[0;31m'
NOCOLOR='\033[0m'

if [ "$EUID" -ne 0 ];
then
  echo -e ""${RED}"[ERROR]: "${NOCOLOR}"TIENES QUE EJECUTAR COMO ROOT!!!"
  exit 1

fi

atriles='/srv/orquesta'

if [ -d $atriles ];
then
  echo "El directorio $atriles si existe"

else
  echo "El directorio $atriles no existe, creando..."
  mkdir -p $atriles

fi

if [ -d "$atriles/Galope" ];
then
  echo "El directorio $atriles/Galope si existe"
  touch $atriles/Galope/flautin.txt
  touch $atriles/Galope/clarinete.txt
  touch $atriles/Galope/corno.txt
  touch $atriles/Galope/trompa.txt
  touch $atriles/Galope/violin.txt
  touch $atriles/Galope/viola.txt
  touch $atriles/Galope/chelo.txt
  touch $atriles/Galope/contrabajo.txt
  touch $atriles/Galope/bateria.txt
  touch $atriles/Galope/xilofono.txt
  touch $atriles/Galope/director.txt
  touch $atriles/Galope/ElSilencio.txt

else
  echo "El directorio $atriles/Galope si existe"
  mkdir -p $atriles/Galope
  touch $atriles/Galope/flautin.txt
  touch $atriles/Galope/clarinete.txt
  touch $atriles/Galope/corno.txt
  touch $atriles/Galope/trompa.txt
  touch $atriles/Galope/violin.txt
  touch $atriles/Galope/viola.txt
  touch $atriles/Galope/chelo.txt
  touch $atriles/Galope/contrabajo.txt
  touch $atriles/Galope/bateria.txt
  touch $atriles/Galope/xilofono.txt
  touch $atriles/Galope/director.txt

fi

if [ -d "$atriles/Carmina" ];
then
  echo "El directorio $atriles/Carmina si existe"
  touch $atriles/Carmina/flautin.txt
  touch $atriles/Carmina/clarinete.txt
  touch $atriles/Carmina/corno.txt
  touch $atriles/Carmina/trompa.txt
  touch $atriles/Carmina/violin.txt
  touch $atriles/Carmina/viola.txt
  touch $atriles/Carmina/chelo.txt
  touch $atriles/Carmina/contrabajo.txt
  touch $atriles/Carmina/bateria.txt
  touch $atriles/Carmina/xilofono.txt
  touch $atriles/Carmina/director.txt
  touch $atriles/Carmina/ElSilencio.txt

else
  echo "El directorio $atriles/Carmina si existe"
  mkdir -p $atriles/Carmina
  touch $atriles/Carmina/flautin.txt
  touch $atriles/Carmina/clarinete.txt
  touch $atriles/Carmina/corno.txt
  touch $atriles/Carmina/trompa.txt
  touch $atriles/Carmina/violin.txt
  touch $atriles/Carmina/viola.txt
  touch $atriles/Carmina/chelo.txt
  touch $atriles/Carmina/contrabajo.txt
  touch $atriles/Carmina/bateria.txt
  touch $atriles/Carmina/xilofono.txt
  touch $atriles/Carmina/director.txt

fi

if [ -d "$atriles/1812" ];
then
  echo "El directorio $atriles/1812 si existe"
  touch $atriles/1812/flautin.txt
  touch $atriles/1812/clarinete.txt
  touch $atriles/1812/corno.txt
  touch $atriles/1812/trompa.txt
  touch $atriles/1812/violin.txt
  touch $atriles/1812/viola.txt
  touch $atriles/1812/chelo.txt
  touch $atriles/1812/contrabajo.txt
  touch $atriles/1812/bateria.txt
  touch $atriles/1812/xilofono.txt
  touch $atriles/1812/director.txt
  touch $atriles/1812/ElSilencio.txt

else
  echo "El directorio $atriles/1812 si existe"
  mkdir -p $atriles/1812
  touch $atriles/1812/flautin.txt
  touch $atriles/1812/clarinete.txt
  touch $atriles/1812/corno.txt
  touch $atriles/1812/trompa.txt
  touch $atriles/1812/violin.txt
  touch $atriles/1812/viola.txt
  touch $atriles/1812/chelo.txt
  touch $atriles/1812/contrabajo.txt
  touch $atriles/1812/bateria.txt
  touch $atriles/1812/xilofono.txt
  touch $atriles/1812/director.txt

fi

if [ -f "users_groups.txt" ];
then
  rm users_groups.txt
  echo "flautin vientomadera,orquesta" >> users_groups.txt
  echo "clarinete vientomadera,orquesta" >> users_groups.txt
  echo "corno vientometal,orquesta" >> users_groups.txt
  echo "trompa vientometal,orquesta" >> users_groups.txt
  echo "violin cuerdas,orquesta" >> users_groups.txt
  echo "viola cuerdas,orquesta" >> users_groups.txt
  echo "chelo cuerdas,orquesta" >> users_groups.txt
  echo "contrabajo cuerdas,orquesta" >> users_groups.txt
  echo "bateria percusion,orquesta" >> users_groups.txt
  echo "xilofono percusion,orquesta" >> users_groups.txt
  echo "director direccion,orquesta" >> users_groups.txt

else
  echo "flautin vientomadera,orquesta" >> users_groups.txt
  echo "clarinete vientomadera,orquesta" >> users_groups.txt
  echo "corno vientometal,orquesta" >> users_groups.txt
  echo "trompa vientometal,orquesta" >> users_groups.txt
  echo "violin cuerdas,orquesta" >> users_groups.txt
  echo "viola cuerdas,orquesta" >> users_groups.txt
  echo "chelo cuerdas,orquesta" >> users_groups.txt
  echo "contrabajo cuerdas,orquesta" >> users_groups.txt
  echo "bateria percusion,orquesta" >> users_groups.txt
  echo "xilofono percusion,orquesta" >> users_groups.txt
  echo "director direccion,orquesta" >> users_groups.txt

fi

while read line;
do
  user=$(echo "$line" | cut -d " " -f1)
  group=$(echo "$line" | cut -d " " -f2 | cut -d "," -f1)
  group2=$(echo "$line" | cut -d " " -f2 | cut -d "," -f2)
  find $atriles/Galope $atriles/Carmina $atriles/1812 -type f \( -name "*.txt" \) -exec chmod 640 {} +
  find $atriles/Galope $atriles/Carmina $atriles/1812 -type f -name "*$user.txt" -exec chown $user:$group {} +
  if [ $user = "director" ];
  then
    find $atriles/Galope $atriles/Carmina $atriles/1812 -type f \( -name "*.txt" \) -exec setfacl -m g:$group:r {} +
  fi
  ls $atriles/Carmina/ > $atriles/Carmina/$user.txt
  cat /home/$user/.bash_history > $atriles/1812/$user

done < users_groups.txt

exit 0
