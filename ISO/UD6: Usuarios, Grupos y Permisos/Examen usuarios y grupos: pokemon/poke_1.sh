#!/bin/bash

RED='\033[0;31m'
NOCOLOR='\033[0m'

if [ "$EUID" -ne 0 ];
then
  echo -e ""${RED}"[ERROR]: "${NOCOLOR}"TIENES QUE EJECUTAR COMO ROOT!!!"
  exit 1

fi

dir='/srv/seniara'

if [ -d "$dir" ];
then
  echo "El directorio $dir si existe"
  if [ -f "$dir/pokeSen.csv" ];
  then
    echo "El fichero pokeSen.csv si existe dentro del directorio $dir"
  else
    echo "El fichero pokeSen.csv no esta descargado"
    find . -name "*pokeSen.csv" -exec mv {} $dir \;
  fi

else
  echo "El directorio $dir no existe, ahora si"
  mkdir -p $dir
fi

propietario=$(ls -la $dir | grep pokeSen.csv | cut -d " " -f3)
uid=$(id -u $propietario)
gid=$(id -g $propietario)

echo "El propietario del fichero pokeSen.csv es $propietario"
echo "Su UID es: $uid"
echo "Su GID es: $gid"
if [[ -r "$dir/pokeSen.csv" ]];
then
  echo "El fichero pokeSen.csv si tiene permisos de lectura"

else
  echo "El fichero pokeSen.csv no tiene permisos de lectura"
fi

exit 0
