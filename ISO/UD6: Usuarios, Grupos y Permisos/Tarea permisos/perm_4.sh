#!/bin/bash

dir=$1
ext=$2
perm=$3

if [ "$EUID" -ne "0" ];
then
   echo "ERROR: tienes que ejecutar como root!!!"
   exit 1
fi

if [ "$#" -ne "3" ];
then
   echo "ERROR: tiene que haber dos argumentos!!!"
   echo "El primer argumento tiene que ser el nombre del directorio"
   echo "El segundo argumento tiene que ser la extension de fichero"
   echo "El tercer argumento tiene que ser los permisos en formato octal"
   exit 1

fi

if [ ! -d "$dir" ]; then
    echo "El directorio $directorio no existe."
    exit 1
fi

find "$dir" -type f -name "*.$ext" -exec chmod "$perm" {} \;

exit 0
