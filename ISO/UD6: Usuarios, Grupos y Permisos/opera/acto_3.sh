#!/bin/bash

RED='\033[0;31m'
NOCOLOR='\033[0m'

if [ "$EUID" -ne 0 ];
then
  echo -e ""${RED}"[ERROR]: "${NOCOLOR}"TIENES QUE EJECUTAR COMO ROOT!!!"
  exit 1

fi

atriles='/srv/orquesta'

if [ -d $atriles ];
then
  echo "El directorio $atriles si existe"

else
  echo "El directorio $atriles no existe, creando..."
  mkdir -p $atriles

fi

if [ -d "$atriles/Nocturnos" ];
then
  echo "El directorio $atriles/Nocturnos si existe"
  touch $atriles/Nocturnos/flautin.txt
  touch $atriles/Nocturnos/clarinete.txt
  touch $atriles/Nocturnos/corno.txt
  touch $atriles/Nocturnos/trompa.txt
  touch $atriles/Nocturnos/violin.txt
  touch $atriles/Nocturnos/viola.txt
  touch $atriles/Nocturnos/chelo.txt
  touch $atriles/Nocturnos/contrabajo.txt
  touch $atriles/Nocturnos/bateria.txt
  touch $atriles/Nocturnos/xilofono.txt
  touch $atriles/Nocturnos/director.txt
  touch $atriles/Nocturnos/ElSilencio.txt

else
  echo "El directorio $atriles/Nocturnos si existe"
  mkdir -p $atriles/Nocturnos
  touch $atriles/Nocturnos/flautin.txt
  touch $atriles/Nocturnos/clarinete.txt
  touch $atriles/Nocturnos/corno.txt
  touch $atriles/Nocturnos/trompa.txt
  touch $atriles/Nocturnos/violin.txt
  touch $atriles/Nocturnos/viola.txt
  touch $atriles/Nocturnos/chelo.txt
  touch $atriles/Nocturnos/contrabajo.txt
  touch $atriles/Nocturnos/bateria.txt
  touch $atriles/Nocturnos/xilofono.txt
  touch $atriles/Nocturnos/director.txt

fi

if [ -d "$atriles/Fratres" ];
then
  echo "El directorio $atriles/Fratres si existe"
  touch $atriles/Fratres/flautin.txt
  touch $atriles/Fratres/clarinete.txt
  touch $atriles/Fratres/corno.txt
  touch $atriles/Fratres/trompa.txt
  touch $atriles/Fratres/violin.txt
  touch $atriles/Fratres/viola.txt
  touch $atriles/Fratres/chelo.txt
  touch $atriles/Fratres/contrabajo.txt
  touch $atriles/Fratres/bateria.txt
  touch $atriles/Fratres/xilofono.txt
  touch $atriles/Fratres/director.txt

else
  echo "El directorio $atriles/Fratres si existe"
  mkdir -p $atriles/Fratres
  touch $atriles/Fratres/flautin.txt
  touch $atriles/Fratres/clarinete.txt
  touch $atriles/Fratres/corno.txt
  touch $atriles/Fratres/trompa.txt
  touch $atriles/Fratres/violin.txt
  touch $atriles/Fratres/viola.txt
  touch $atriles/Fratres/chelo.txt
  touch $atriles/Fratres/contrabajo.txt
  touch $atriles/Fratres/bateria.txt
  touch $atriles/Fratres/xilofono.txt
  touch $atriles/Fratres/director.txt

fi

if [ -d "$atriles/Adagio" ];
then
  echo "El directorio $atriles/Adagio si existe"
  touch $atriles/Adagio/flautin.txt
  touch $atriles/Adagio/clarinete.txt
  touch $atriles/Adagio/corno.txt
  touch $atriles/Adagio/trompa.txt
  touch $atriles/Adagio/violin.txt
  touch $atriles/Adagio/viola.txt
  touch $atriles/Adagio/chelo.txt
  touch $atriles/Adagio/contrabajo.txt
  touch $atriles/Adagio/bateria.txt
  touch $atriles/Adagio/xilofono.txt
  touch $atriles/Adagio/director.txt
  touch $atriles/Adagio/ElSilencio.txt

else
  echo "El directorio $atriles/Adagio si existe"
  mkdir -p $atriles/Adagio
  touch $atriles/Adagio/flautin.txt
  touch $atriles/Adagio/clarinete.txt
  touch $atriles/Adagio/corno.txt
  touch $atriles/Adagio/trompa.txt
  touch $atriles/Adagio/violin.txt
  touch $atriles/Adagio/viola.txt
  touch $atriles/Adagio/chelo.txt
  touch $atriles/Adagio/contrabajo.txt
  touch $atriles/Adagio/bateria.txt
  touch $atriles/Adagio/xilofono.txt
  touch $atriles/Adagio/director.txt

fi

if [ -d "$atriles/DeProfundis" ];
then
  echo "El directorio $atriles/DeProfundis si existe"
  touch $atriles/DeProfundis/flautin.txt
  touch $atriles/DeProfundis/clarinete.txt
  touch $atriles/DeProfundis/corno.txt
  touch $atriles/DeProfundis/trompa.txt
  touch $atriles/DeProfundis/violin.txt
  touch $atriles/DeProfundis/viola.txt
  touch $atriles/DeProfundis/chelo.txt
  touch $atriles/DeProfundis/contrabajo.txt
  touch $atriles/DeProfundis/bateria.txt
  touch $atriles/DeProfundis/xilofono.txt
  touch $atriles/DeProfundis/director.txt
  touch $atriles/DeProfundis/ElSilencio.txt

else
  echo "El directorio $atriles/DeProfundis si existe"
  mkdir -p $atriles/DeProfundis
  touch $atriles/DeProfundis/flautin.txt
  touch $atriles/DeProfundis/clarinete.txt
  touch $atriles/DeProfundis/corno.txt
  touch $atriles/DeProfundis/trompa.txt
  touch $atriles/DeProfundis/violin.txt
  touch $atriles/DeProfundis/viola.txt
  touch $atriles/DeProfundis/chelo.txt
  touch $atriles/DeProfundis/contrabajo.txt
  touch $atriles/DeProfundis/bateria.txt
  touch $atriles/DeProfundis/xilofono.txt
  touch $atriles/DeProfundis/director.txt

fi

if [ -f "users_groups.txt" ];
then
  rm users_groups.txt
  echo "flautin vientomadera,orquesta" >> users_groups.txt
  echo "clarinete vientomadera,orquesta" >> users_groups.txt
  echo "corno vientometal,orquesta" >> users_groups.txt
  echo "trompa vientometal,orquesta" >> users_groups.txt
  echo "violin cuerdas,orquesta" >> users_groups.txt
  echo "viola cuerdas,orquesta" >> users_groups.txt
  echo "chelo cuerdas,orquesta" >> users_groups.txt
  echo "contrabajo cuerdas,orquesta" >> users_groups.txt
  echo "bateria percusion,orquesta" >> users_groups.txt
  echo "xilofono percusion,orquesta" >> users_groups.txt
  echo "director direccion,orquesta" >> users_groups.txt

else
  echo "flautin vientomadera,orquesta" >> users_groups.txt
  echo "clarinete vientomadera,orquesta" >> users_groups.txt
  echo "corno vientometal,orquesta" >> users_groups.txt
  echo "trompa vientometal,orquesta" >> users_groups.txt
  echo "violin cuerdas,orquesta" >> users_groups.txt
  echo "viola cuerdas,orquesta" >> users_groups.txt
  echo "chelo cuerdas,orquesta" >> users_groups.txt
  echo "contrabajo cuerdas,orquesta" >> users_groups.txt
  echo "bateria percusion,orquesta" >> users_groups.txt
  echo "xilofono percusion,orquesta" >> users_groups.txt
  echo "director direccion,orquesta" >> users_groups.txt

fi

while read line;
do
  user=$(echo "$line" | cut -d " " -f1)
  group=$(echo "$line" | cut -d " " -f2 | cut -d "," -f1)
  group2=$(echo "$line" | cut -d " " -f2 | cut -d "," -f2)
  find $atriles/Nocturnos $atriles/Fratres $atriles/Adagio $atriles/DeProfundis -type f \( -name "*.txt" \) -exec chmod 640 {} +
  find $atriles/Nocturnos $atriles/Fratres $atriles/Adagio $atriles/DeProfundis -type f -name "*$user.txt" -exec chown $user:$group {} +
  if [ $user = "director" ];
  then
    find $atriles/Nocturnos $atriles/Fratres $atriles/Adagio $atriles/DeProfundis -type f \( -name "*.txt" \) -exec setfacl -m g:$group:r {} +
    find $atriles/Adagio -type f -name "*.txt" -exec chown director:orquesta {} +
    chown $user:$group2 $atriles/Adagio/
  fi
  touch -t 198310010000 $atriles/Nocturnos/*
  chgrp $group2 $atriles/Fratres/
  chmod g+s $atriles/Fratres/
  find $atriles/Fratres/ -type f -name "*.txt" -exec chown :$group2 {} +
  chmod g+s $atriles/Adagio
  chmod u+s $atriles/Adagio
  for users in $(echo "$line" | cut -d " " -f1);
  do
    uid=$(id "$user" | cut -d " " -f1 | cut -d "=" -f2 | cut -d "(" -f1)
    gid=$(id "$user" | cut -d " " -f3 | cut -d "," -f2 | cut -d "(" -f1)
    echo "UID: $uid" >> $atriles/DeProfundis/$users
    echo "GID: $gid" >> $atriles/DeProfundis/$users
  done

done < users_groups.txt

exit 0
