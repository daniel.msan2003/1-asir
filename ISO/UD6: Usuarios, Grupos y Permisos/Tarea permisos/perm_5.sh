#!/bin/bash

maquina=$(cat /etc/hostname)

if [ "$EUID" -ne "0" ];
then
   echo "ERROR: tienes que ejecutar como root!!!"
   exit 1
fi

if [ "$maquina" = "Empleado" ];
then
   # Los permisos predeterminados para archivos seran 644 (rw-r--r--) y para directorios seran 755 (rwxr-xr-x)
   umask 0022

elif [ "$maquina" = "ServidorWeb" ];
then
   # Los permisos predeterminados para archivos seran 660 (rw-rw----) y para directorios seran 770 (rwxrwx---)
   umask 0007

elif [ "$maquina" = "Empleado" ];
then
   # Los permisos predeterminados para archivos seran 644 (rw-r--r--) y para directorios seran 755 (rwxr-xr-x)
   umask 0644

fi

exit 0
