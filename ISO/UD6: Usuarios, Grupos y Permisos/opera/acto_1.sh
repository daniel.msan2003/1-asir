#!/bin/bash

RED='\033[0;31m'
NOCOLOR='\033[0m'

if [ "$EUID" -ne 0 ];
then
  echo -e ""${RED}"[ERROR]: "${NOCOLOR}"TIENES QUE EJECUTAR COMO ROOT!!!"
  exit 1

fi

atriles='/srv/orquesta'

if [ -d $atriles ];
then
  echo "El directorio $atriles si existe"

else
  echo "El directorio $atriles no existe, creando..."
  mkdir -p $atriles

fi

if [ -d "$atriles/LaGranPuertaDeKiev" ];
then
  echo "El directorio $atriles/LaGranPuertaDeKiev si existe"
  echo "flautin" > $atriles/LaGranPuertaDeKiev/flautin.txt
  echo "clarinete" > $atriles/LaGranPuertaDeKiev/clarinete.txt
  echo "corno" > $atriles/LaGranPuertaDeKiev/corno.txt
  echo "trompa" > $atriles/LaGranPuertaDeKiev/trompa.txt
  echo "violin" > $atriles/LaGranPuertaDeKiev/violin.txt
  echo "viola" > $atriles/LaGranPuertaDeKiev/viola.txt
  echo "chelo" > $atriles/LaGranPuertaDeKiev/chelo.txt
  echo "contrabajo" > $atriles/LaGranPuertaDeKiev/contrabajo.txt
  echo "bateria" > $atriles/LaGranPuertaDeKiev/bateria.txt
  echo "xilofono" > $atriles/LaGranPuertaDeKiev/xilofono.txt
  echo "director" > $atriles/LaGranPuertaDeKiev/director.txt


else
  echo "El directorio $atriles/LaGranPuertaDeKiev no existe, creando..."
  mkdir -p $atriles/LaGranPuertaDeKiev
  echo "flautin" > $atriles/LaGranPuertaDeKiev/flautin.txt
  echo "clarinete" > $atriles/LaGranPuertaDeKiev/clarinete.txt
  echo "corno" > $atriles/LaGranPuertaDeKiev/corno.txt
  echo "trompa" > $atriles/LaGranPuertaDeKiev/trompa.txt
  echo "violin" > $atriles/LaGranPuertaDeKiev/violin.txt
  echo "viola" > $atriles/LaGranPuertaDeKiev/viola.txt
  echo "chelo" > $atriles/LaGranPuertaDeKiev/chelo.txt
  echo "contrabajo" > $atriles/LaGranPuertaDeKiev/contrabajo.txt
  echo "bateria" > $atriles/LaGranPuertaDeKiev/bateria.txt
  echo "xilofono" > $atriles/LaGranPuertaDeKiev/xilofono.txt
  echo "director" > $atriles/LaGranPuertaDeKiev/director.txt


fi

if [ -d "$atriles/DanubioAzul" ];
then
  echo "El directorio $atriles/DanubioAzul si existe"
  echo "flautin" > $atriles/DanubioAzul/flautin.txt
  echo "clarinete" > $atriles/DanubioAzul/clarinete.txt
  echo "corno" > $atriles/DanubioAzul/corno.txt
  echo "trompa" > $atriles/DanubioAzul/trompa.txt
  echo "violin" > $atriles/DanubioAzul/violin.txt
  echo "viola" > $atriles/DanubioAzul/viola.txt
  echo "chelo" > $atriles/DanubioAzul/chelo.txt
  echo "contrabajo" > $atriles/DanubioAzul/contrabajo.txt
  echo "bateria" > $atriles/DanubioAzul/bateria.txt
  echo "xilofono" > $atriles/DanubioAzul/xilofono.txt
  echo "director" > $atriles/DanubioAzul/director.txt

else
  echo "El directorio $atriles/DanubioAzul no existe, creando..."
  mkdir -p $atriles/DanubioAzul
  echo "flautin" > $atriles/DanubioAzul/flautin.txt
  echo "clarinete" > $atriles/DanubioAzul/clarinete.txt
  echo "corno" > $atriles/DanubioAzul/corno.txt
  echo "trompa" > $atriles/DanubioAzul/trompa.txt
  echo "violin" > $atriles/DanubioAzul/violin.txt
  echo "viola" > $atriles/DanubioAzul/viola.txt
  echo "chelo" > $atriles/DanubioAzul/chelo.txt
  echo "contrabajo" > $atriles/DanubioAzul/contrabajo.txt
  echo "bateria" > $atriles/DanubioAzul/bateria.txt
  echo "xilofono" > $atriles/DanubioAzul/xilofono.txt
  echo "director" > $atriles/DanubioAzul/director.txt


fi

if [ -d "$atriles/SinfoniaDelNuevoMundo" ];
then
  echo "El directorio $atriles/SinfoniaDelNuevoMundo si existe"
  echo "flautin" > $atriles/SinfoniaDelNuevoMundo/flautin.txt
  echo "clarinete" > $atriles/SinfoniaDelNuevoMundo/clarinete.txt
  echo "corno" > $atriles/SinfoniaDelNuevoMundo/corno.txt
  echo "trompa" > $atriles/SinfoniaDelNuevoMundo/trompa.txt
  echo "violin" > $atriles/SinfoniaDelNuevoMundo/violin.txt
  echo "viola" > $atriles/SinfoniaDelNuevoMundo/viola.txt
  echo "chelo" > $atriles/SinfoniaDelNuevoMundo/chelo.txt
  echo "contrabajo" > $atriles/SinfoniaDelNuevoMundo/contrabajo.txt
  echo "bateria" > $atriles/SinfoniaDelNuevoMundo/bateria.txt
  echo "xilofono" > $atriles/SinfoniaDelNuevoMundo/xilofono.txt
  echo "director" > $atriles/SinfoniaDelNuevoMundo/director.txt

else
  echo "El directorio $atriles/SinfoniaDelNuevoMundo no existe, creando..."
  mkdir -p $atriles/SinfoniaDelNuevoMundo
  echo "flautin" > $atriles/SinfoniaDelNuevoMundo/flautin.txt
  echo "clarinete" > $atriles/SinfoniaDelNuevoMundo/clarinete.txt
  echo "corno" > $atriles/SinfoniaDelNuevoMundo/corno.txt
  echo "trompa" > $atriles/SinfoniaDelNuevoMundo/trompa.txt
  echo "violin" > $atriles/SinfoniaDelNuevoMundo/violin.txt
  echo "viola" > $atriles/SinfoniaDelNuevoMundo/viola.txt
  echo "chelo" > $atriles/SinfoniaDelNuevoMundo/chelo.txt
  echo "contrabajo" > $atriles/SinfoniaDelNuevoMundo/contrabajo.txt
  echo "bateria" > $atriles/SinfoniaDelNuevoMundo/bateria.txt
  echo "xilofono" > $atriles/SinfoniaDelNuevoMundo/xilofono.txt
  echo "director" > $atriles/SinfoniaDelNuevoMundo/director.txt

fi

if [ -d "$atriles/SuiteDeJazz" ];
then
  echo "El directorio $atriles/SuiteDeJazz si existe"
  echo "flautin" > $atriles/SuiteDeJazz/flautin.txt
  echo "clarinete" > $atriles/SuiteDeJazz/clarinete.txt
  echo "corno" > $atriles/SuiteDeJazz/corno.txt
  echo "trompa" > $atriles/SuiteDeJazz/trompa.txt
  echo "violin" > $atriles/SuiteDeJazz/violin.txt
  echo "viola" > $atriles/SuiteDeJazz/viola.txt
  echo "chelo" > $atriles/SuiteDeJazz/chelo.txt
  echo "contrabajo" > $atriles/SuiteDeJazz/contrabajo.txt
  echo "bateria" > $atriles/SuiteDeJazz/bateria.txt
  echo "xilofono" > $atriles/SuiteDeJazz/xilofono.txt
  echo "director" > $atriles/SuiteDeJazz/director.txt


else
  echo "El directorio $atriles/SuiteDeJazz no existe, creando..."
  mkdir -p $atriles/SuiteDeJazz
  echo "flautin" > $atriles/SuiteDeJazz/flautin.txt
  echo "clarinete" > $atriles/SuiteDeJazz/clarinete.txt
  echo "corno" > $atriles/SuiteDeJazz/corno.txt
  echo "trompa" > $atriles/SuiteDeJazz/trompa.txt
  echo "violin" > $atriles/SuiteDeJazz/violin.txt
  echo "viola" > $atriles/SuiteDeJazz/viola.txt
  echo "chelo" > $atriles/SuiteDeJazz/chelo.txt
  echo "contrabajo" > $atriles/SuiteDeJazz/contrabajo.txt
  echo "bateria" > $atriles/SuiteDeJazz/bateria.txt
  echo "xilofono" > $atriles/SuiteDeJazz/xilofono.txt
  echo "director" > $atriles/SuiteDeJazz/director.txt

fi

if [ -f "users_groups.txt" ];
then
  rm users_groups.txt
  echo "flautin vientomadera,orquesta" >> users_groups.txt
  echo "clarinete vientomadera,orquesta" >> users_groups.txt
  echo "corno vientometal,orquesta" >> users_groups.txt
  echo "trompa vientometal,orquesta" >> users_groups.txt
  echo "violin cuerdas,orquesta" >> users_groups.txt
  echo "viola cuerdas,orquesta" >> users_groups.txt
  echo "chelo cuerdas,orquesta" >> users_groups.txt
  echo "contrabajo cuerdas,orquesta" >> users_groups.txt
  echo "bateria percusion,orquesta" >> users_groups.txt
  echo "xilofono percusion,orquesta" >> users_groups.txt
  echo "director direccion,orquesta" >> users_groups.txt

else
  echo "flautin vientomadera,orquesta" >> users_groups.txt
  echo "clarinete vientomadera,orquesta" >> users_groups.txt
  echo "corno vientometal,orquesta" >> users_groups.txt
  echo "trompa vientometal,orquesta" >> users_groups.txt
  echo "violin cuerdas,orquesta" >> users_groups.txt
  echo "viola cuerdas,orquesta" >> users_groups.txt
  echo "chelo cuerdas,orquesta" >> users_groups.txt
  echo "contrabajo cuerdas,orquesta" >> users_groups.txt
  echo "bateria percusion,orquesta" >> users_groups.txt
  echo "xilofono percusion,orquesta" >> users_groups.txt
  echo "director direccion,orquesta" >> users_groups.txt

fi

while read line;
do
  user=$(echo "$line" | cut -d " " -f1)
  group=$(echo "$line" | cut -d " " -f2 | cut -d "," -f1)
  find $atriles -type f \( -name "*.txt" \) -exec chmod 640 {} +
  find $atriles -type f -name "*$user.txt" -exec chown $user:$group {} +
  if [ $user = "director" ];
  then
    find $atriles -type f \( -name "*.txt" \) -exec setfacl -m g:$group:r {} +
  fi

done < users_groups.txt

exit 0
