#!/bin/bash

file=$1
user=$2
perm=$3

if [ "$EUID" -ne "0" ];
then
   echo "ERROR: tienes que ejecutar como root!!!"
   exit 1
fi

if [ "$#" -ne "3" ];
then
   echo "ERROR: tiene que haber 3 argumentos!!!"
   echo "El primero tiene que ser el nombre del fichero"
   echo "El segundo tiene que ser el nombre del usuario"
   echo "El tercero tiene que ser los permisos"
   exit 1
fi

if [ ! -f "$file" ];
then
    echo "El archivo $file no existe."
    exit 1
fi

echo "Cambiando propietario de $file a $user y permisos a $perm ..."
sleep 1
chown $user $file
chmod $perm $file
echo "Propietario y permisos cambiados correctamente."

exit 0
