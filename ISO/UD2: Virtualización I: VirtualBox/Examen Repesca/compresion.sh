#!/bin/bash

if [ -f "resultado.txt" ];
then
   rm resultado.txt
fi

  for numero in $(seq 1 1000000);
  do
    x=($RANDOM)
    let op1=x*x
    let op2=5*x
    let op3=2*x+1
    let resultado=op1+op2-op3
    echo "$resultado" >> resultado.txt
    zip resultado.zip resultado.txt
    let com1=$(du -b resultado.txt | cut -d "" -f1)
    let com2=$(du -b resultado.zip)
    echo "El tamaño de bytes del archivo resultado.txt son $com1"
    echo "El tamaño de bytes del archivo resultado.zip son $com2"
  done

exit 0
