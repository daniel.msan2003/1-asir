#!/bin/bash

if [ "$EUID" -ne 0 ];
then
   echo "ERROR: TIENES QUE EJECUTAR COMO ROOT!!"
   exit 1
fi

file='rivendel.csv'
password='sauron'
linea=$(cat "$file" | wc -l)
let linea=linea-1

cat "$file" | tail -n "$linea" | while read line;
do
  user=$(echo "$line" | cut -d " " -f1 | cut -d "," -f1 | tr '[:upper:]' '[:lower:]')
  full_name=$(echo "$line" | cut -d "," -f1)
  group=$(echo "$line" | cut -d "," -f2)
  value=$(cat /etc/passwd | cut -d ":" -f1 | grep -w "$user" | wc -l)
  if [ "$value" -eq 1 ];
  then
     echo "El usuario $user ya existe"

  else
     useradd $user -m -d "/home/$group/$user" -s "/bin/bash" -G $group -c "$full_name"
     echo -e "$password\n$password" | passwd $user
     echo "Se ha creado el usuario $user con la contrasenya $password"

  fi

done

exit 0
