#!/bin/bash

user=$(whoami)

if [ "$EUID" -eq "0" ];
then
    echo "ERROR: tienes que ejecutar este script como root!!!"
    exit 1

fi

if [ "$#" -eq "0" -o "$#" -gt "1" ];
then
   echo "ERROR: Solo puede haber un parámetro!!!"
   exit 1
fi

proceso=$1
pkill -f "$proceso"

if [ "$?" -eq "0" ];
then
   echo "Los procesos fueron terminados correctamente"

else
   echo "No se pudieron terminar los procesos"
   exit 1

fi

exit 0
