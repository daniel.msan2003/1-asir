#!/bin/bash

proceso1=$(pgrep -f imprime-normal)
proceso2=$(pgrep -f imprime-3d)

pgrep -f "imprime-normal" | pgrep -f "imprime-3d" || rc=1

if [ "$rc" = 0 ];
then
   echo "Los procesos ya estan en ejecucion"
   exit 1

else
   renice -n -5 -p $proceso1 && renice -n -5 -p $proceso2
   nice -n -5 ./imprime-normal.sh &
   nice -n -5 ./imprimee-3d.sh &

fi

exit 0
