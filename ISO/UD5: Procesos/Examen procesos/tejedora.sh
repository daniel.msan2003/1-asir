#!/bin/bash

# La variable aranya tiene un valor de 1
aranya=1

# La variable horda genera un numero aleatorio desde el 1 al 30
horda=$(( (RANDOM % 30) + 1 ))

rm -rf aranya* 2> /dev/null

# El while comprueba si la variable aranya que tiene el valor 1, sea menor o igual al numero aleatorio de la variable horda
while [ $aranya -le $horda ]; do
    name="aranya_$aranya"

	# La variable trabajo tiene un generador aleatorio del 1 al 3600
	trabajo=$(( (RANDOM % 3600) + 1 ))

	# Se genera un script llamado "aranya_1.sh" y mete el contenido al script con un echo
	echo '#!/bin/bash' >> $name.sh
	echo "sleep $trabajo" >> $name.sh

	# Se da permisos de ejecucion al script
	chmod +x $name.sh

    # Se ejecuta el script de segundo plano
    ./$name.sh &

    # La variable interval tiene un generador aleatorio del 1 al 100
    interval=$(( (RANDOM % 100) + 1 ))

	# Ahora la variable aranya va a tener un valor diferente porque va a sumar 1 cada vez
	let aranya=$aranya+1

    # Va a dormir un numero aleatorio del 1 al 100 segundos
    sleep $interval

	# Despues del comando sleep, va a borrar el script creado
	rm -rf $name.sh

done

exit 0
