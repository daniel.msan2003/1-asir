#!/bin/bash

if [ "$EUID" -ne 0 ];
then
   echo "ERROR: TIENES QUE EJECUTAR COMO ROOT!!"
   exit 1
fi

file='rivendel.csv'
linea=$(cat "$file" | wc -l)
let linea=linea-1

cat "$file" | tail -n "$linea" | cut -d "," -f2 | sort | uniq | while read line;
do
  razaG=$(echo "$line" | cut -d "," -f2)
  group=$(cat /etc/group | cut -d ":" -f1 | grep "$razaG" | wc -l)
  if [ "$group" -eq 0 ];
  then
     echo "El grupo $razaG no existe, ahora se ha creado"
     groupadd "$razaG"

  else
     echo "El grupo $razaG ya existe"

  fi

done

exit 0
