#!/bin/bash

primo=true

  read -p "Por favor, ingresa un numero: " num

  if [ "$num" -le "1" ];
  then
    primo=false

  fi

  for ((i = 2; i * i <= $num; i++));
  do
    if [ $(( $num % i )) -eq 0 ];
    then
      primo=false
      break
    fi
  done

  if [ "$primo" = "true" ];
  then
    echo "$num es un numero primo."

  else
    echo "$num no es un numero primo."

exit 0
