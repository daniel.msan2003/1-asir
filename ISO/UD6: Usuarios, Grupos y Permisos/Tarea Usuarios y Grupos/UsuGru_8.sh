#!/bin/bash

file='/etc/group'

while read line;
do
  group=$(echo "$line" | cut -d ":" -f1)
  members=$(echo "$line" | cut -d ":" -f4)
  if [[ -z "$members" ]];
  then
     echo "El grupo $group no tiene miembros"
  fi

done < "$file"

exit 0
