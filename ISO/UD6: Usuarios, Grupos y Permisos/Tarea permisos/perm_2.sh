#!/bin/bash

dir=$1
perm=$2

if [ "$EUID" -ne "0" ];
then
   echo "ERROR: se tiene que ejecutar como root!!!"
   exit 1
fi

if [ "$#" -ne "2" ];
then
    echo "ERROR: tiene que haber dos argumentos!!!"
    echo "El primer argumento tiene que ser un directorio (con subdirectorios)"
    exit 1
fi

if [ ! -d "$dir" ];
then
    echo "ERROR: el directorio $dir no existe."
    exit 1
fi

echo "Cambiando permisos de $dir a $perm de forma recursiva..."
sleep 1

find "$dir" -type d -exec chmod "$perm" {} +
find "$dir" -type f -exec chmod "$perm" {} +

echo "Se han cambiado los permisos correctamente"

exit 0
