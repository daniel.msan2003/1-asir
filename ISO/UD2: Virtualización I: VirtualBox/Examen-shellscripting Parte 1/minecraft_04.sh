#!/bin/bash

  if [ -f "latest.log" ];
  then
     cat latest.log | grep "INFO" >> info.dat
     cat latest.log | grep "WARNING" >> warning.dat
     cat latest.log | grep "ERROR" >> error.dat

  else
     echo "No tienes el fichero latest.log"
     exit 1
  fi

exit 0
