#!/bin/bash

validar_ip() {
    local ip=$1
    local form="^([0-9]{1,3}\.){3}[0-9]{1,3}$"
    if [[ ! $ip =~ $form ]];
    then
        echo "ERROR: La siguiente IP no es valida: "$ip""
        exit 1
    fi
}

validar_mascara() {
    local mask=$1
    local form="^[0-9]{1,2}$"
    if [[ ! $mask =~ $form ]] || [ $mask -gt 32 ];
    then
        echo "ERROR: La siguiente mascara no es valida: "$mask""
        exit 1
    fi
}

if [ "$#" -ne 2 ];
then
    echo "ERROR: debe haber dos argumentos con dos IPs diferentes y sus mascaras."
    exit 1
fi

ip1=$(echo "$1" | cut -d '/' -f1)
mask1=$(echo "$1" | cut -d '/' -f2)
ip2=$(echo "$2" | cut -d '/' -f1)
mask2=$(echo "$2" | cut -d '/' -f2)

validar_ip "$ip1"
validar_mascara "$mask1"
validar_ip "$ip2"
validar_mascara "$mask2"

network1=$(echo "$ip1" | cut -d '.' -f1-3).0
network2=$(echo "$ip2" | cut -d '.' -f1-3).0

if [ $network1 == $network2 ] && [ $mask1 == $mask2 ];
then
    echo "Las direcciones pertenecen a la misma red,"
else
    echo "Las direcciones no pertenecen a la misma red."
fi

exit 0
