#!/bin/bash

contenedor=$(docker ps | grep physical | wc -l)

if [ $EUID -ne "0" ];
then
   echo "ERROR: TIENES QUE EJECUTAR COMO ROOT!!"
   exit 1
fi

if [ $# -ne "2" ];
then
    echo "ERROR: tienes que añadir dos campos al ejecutar el script: ./mision-01.sh [Nombre_Contenedor] [Imagen]"
    exit

else
   if [ $1 = "physical" ];
   then
     if [ $contenedor = "1" ];
     then
        echo "El contenedor physical ya existe, borrando..."
        docker rm -f $1
        sleep 1
        echo "Ya se ha borrado"
        exit 1

     elif [ $contenedor = "0" ];
     then
        echo "Creando el contenedor $1 con imagen $2..."
        docker run -d --name $1 $2 sleep infinity
        docker exec physical mkdir -p ./ROVE/Cameras
        docker exec physical touch ./ROVE/Cameras/engaged
        docker exec physical mkdir -p ./ROVE/Laser
        docker exec physical touch ./ROVE/Laser/engaged
        docker exec physical mkdir -p ./ROVE/Sonars
        docker exec physical touch ./ROVE/Sonars/engaged
        docker exec physical mkdir -p ./ROVE/Motors
        docker exec physical touch ./ROVE/Motors/engaged
        docker exec physical ls ROVE/Cameras
        docker exec physical ls ROVE/Laser
        docker exec physical ls ROVE/Sonars
        docker exec physical ls ROVE/Motors

     fi

   else
      echo "ERROR: El primer parametro debe ser physical."
      exit 1
   fi

fi

exit 0
