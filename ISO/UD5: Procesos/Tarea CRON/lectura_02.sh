#!/bin/bash

# Este script comprueba cuales servicios estan habilitados y deshabilitados

# La variable "fich" contiene la ruta del fichero que tiene una lista de todos los servicios
fich="/servicios/lista_de_servicios.txt"

# Comprueba que el fichero con todos los servicios existe, si no existe da error
if [ ! -f "$fich" ]; then
    echo "Error: El archivo de servicios no existe."
    exit 1
fi

# La variable "serv" muestra el contenido de la variable "fich" que tiene el fichero con los servicios
serv=$(cat "$fich")

# El "for" lee las lineas que tienen los nombres de los servicios
for item in $serv; do
    # Comprueba que los servicios estan habilitados
    if systemctl is-active --quiet $item; then
       echo "El servicio $item está habilitado."
    else
       # Si no esta habilitado, manda un mensaje al log del sistema quee dice que el servicio no esta habilitado
       mensaje="ADVERTENCIA: El servicio $item no está habilitado."
       echo $mensaje | logger -p user.warning
    fi
done
