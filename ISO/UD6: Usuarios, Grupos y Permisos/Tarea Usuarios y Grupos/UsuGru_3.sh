#!/bin/bash

file='/etc/passwd'
arg=$1

while read line;
do
  user=$(echo "$line" | cut -d ":" -f1)
  shell=$(echo "$line" | cut -d ":" -f7)
  if [ "$arg" = "$shell" ];
  then
     echo "$user --> $shell"

  fi

done < "$file"

exit 0
