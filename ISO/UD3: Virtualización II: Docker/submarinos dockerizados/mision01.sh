#!/bin/bash

typhoon="Typhoon: eslora de 170 metros, una manga de 13,5 m y un calado de 10 m, integran un reactor nuclear de agua a presión 650 bares y una turbina de vapor AEU con una helice que alcanza velocidades de 15 nudos en la superficie y 29 nudos bajo el agua. Armamento: Posee 3 misiles Poseidon"
delta="Delta: eslora de 166 m, una manga de 12,3 m y un calado de 8,8 m, incluye dos reactores de agua a presión y dos turbinas de vapor que accionan dos hélices de cinco palas, alcanzando una velocidad bajo el agua de 24 nudos. Armamento: Posee 4 lanzatorpedos, y 4 misiles balisticos"
vanguard="Vanguard: eslora de 149,9 m, una manga de 12,8 m y un calado de 12 m, incluyendo un reactor de agua a presión suministrado por Rolls-Royce, dos turbinas 20,5 MW fabricadas por GEC, dos motores propulsores retráctiles auxiliares, dos turbogeneradores y dos alternadores diésel que lo habilitan para alcanzar una velocidad bajo el agua de 25 nudos. Armamento: Posee 5 misiles trident II, y 1 prototipo secreto."

if [ "$EUID" -ne 0 ];
then
   echo "ERROR: TIENES QUE EJECUTAR COMO ROOT!!"
   exit 1
fi

# Aqui creo el contenedor de la Clase de submarino Typhoon
docker create -it --name Typhoon ubuntu bash && docker start Typhoon
docker exec Typhoon mkdir -p ./puestaEnMarcha/MisilPoseidon01_OFF ./puestaEnMarcha/MisilPoseidon02_OFF ./puestaEnMarcha/MisilPoseidon03_OFF
docker exec Typhoon touch ./puestaEnMarcha/subStats.txt
docker exec Typhoon bash -c "echo '$typhoon' > ./puestaEnMarcha/subStats.txt"

# Aqui creo el contenedor de la Clase de submarino Delta
docker create -it --name Delta ubuntu bash && docker start Delta
docker exec Delta mkdir -p ./puestaEnMarcha/Lanzatorpedo01_OFF ./puestaEnMarcha/Lanzatorpedo02_OFF ./puestaEnMarcha/Lanzatorpedo03_OFF ./puestaEnMarcha/Lanzatorpedo04_OFF ./puestaEnMarcha/MisileBalisticos01_OFF ./puestaEnMarcha/MisileBalisticos02_OFF ./puestaEnMarcha/MisileBalisticos03_OFF ./puestaEnMarcha/MisileBalisticos04_OFF
docker exec Delta touch ./puestaEnMarcha/subStats.txt
docker exec Delta bash -c "echo '$delta' > ./puestaEnMarcha/subStats.txt"

# Aqui creo el contenedor de la Clase de submarino Vanguard
docker create -it --name Vanguard ubuntu bash && docker start Vanguard
docker exec Vanguard mkdir -p ./puestaEnMarcha/MisilTridentII01_OFF ./puestaEnMarcha/MisilTridentII02_OFF ./puestaEnMarcha/MisilTridentII03_OFF ./puestaEnMarcha/MisilTridentII04_OFF ./puestaEnMarcha/MisilTridentII05_OFF ./puestaEnMarcha/PrototipoSecreto01_OFF
docker exec Vanguard touch ./puestaEnMarcha/subStats.txt
docker exec Vanguard bash -c "echo '$vanguard' > ./puestaEnMarcha/subStats.txt"

exit 0
