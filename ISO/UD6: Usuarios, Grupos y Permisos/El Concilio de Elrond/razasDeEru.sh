#!/bin/bash

if [ "$EUID" -ne 0 ];
then
   echo "ERROR: TIENES QUE EJECUTAR COMO ROOT!!"
   exit 1
fi

file='rivendel.csv'
linea=$(cat "$file" | wc -l)
let linea=linea-1

cat "$file" | tail -n "$linea" | while read line;
do
  razaG=$(echo "$line" | cut -d "," -f2)
  compr=$(cat /etc/group | cut -d ":" -f1 | grep "$razaG" | wc -l)
  if [ "$compr" -eq 0 ];
  then
     echo "Se ha creado el grupo $razaG"
     groupadd "$razaG"

  fi

done

exit 0
