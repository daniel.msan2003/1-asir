#!/bin/bash

  read -p "Indica la tabla de multiplicar: " num1
  read -p "Indique los elementos: " num2

  echo "+-----------------------------+"
  echo "      Tabla de $num1"
  echo "+-----------------------------+"
  for ((i=1; i<=num2; i++))
  do
    result=$((num1*i))
    echo "$i x $num1 = $result"
  done

exit 0
